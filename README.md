Sestim is a multi-platform desktop application for keeping records of software estimates.

Screenshots
-------------------------------------------------------------------------------
<a href="https://gitlab.com/nilit/sestim/raw/master/docs/project-list.png">
    <img src="https://gitlab.com/nilit/sestim/raw/master/docs/project-list.png" width="377px">
</a>
<a href="https://gitlab.com/nilit/sestim/raw/master/docs/project-edit.png">
    <img src="https://gitlab.com/nilit/sestim/raw/master/docs/project-edit.png" width="377px">
</a>
<a href="https://gitlab.com/nilit/sestim/raw/master/docs/story-edit.png">
    <img src="https://gitlab.com/nilit/sestim/raw/master/docs/story-edit.png" width="377px">
</a>
<a href="https://gitlab.com/nilit/sestim/raw/master/docs/project-estimate-edit.png">
    <img src="https://gitlab.com/nilit/sestim/raw/master/docs/project-estimate-edit.png" width="377px">
</a>

Motivation
-------------------------------------------------------------------------------
I suppose if you have ever tried to create a bottom up estimate for a project by hand, you know that sometimes it can be quite daunting. You might end up with a complicated file structure for keeping track of your estimates, and a regular need to recalculate them *by hand*. I'm talking about files like the following one (only much more sophisticated):

```yaml
project:
    name: POS System
    
    estimates:
        - {best: 100, worst: 400, expected: 200, date_created: 2017-07-09}
        - {best: 110, worst: 350, expected: 180, date_created: 2017-08-09}
    
    stories:
        
        handle_returns:
            size: large
            estiamtes:
                - {best: 30, worst: 80, expected: 55, date_created: 2017-08-09}
        
        porcess_sale:
            size: medium
            estiamtes:
                - {best: 10, worst: 20, expected: 40, date_created: 2017-08-09}
                - {best: 15, worst: 23, expected: 30, date_created: 2017-09-09}
```

Writing all of that by hand and manually summing it up later is no fun. It is possible to find several enterprise applications that address exactly that problem, but I wasn't able to find anything free & multi-platform. So I decided to investigate the possibility of creating a simple app for that very purpose (and in the meantime try out kotlin & tornadofx in practice).

Project Status
-------------------------------------------------------------------------------
At the moment it's mostly just a proof of concept. And although it's pretty stable and decently tested (with unit and integration tests) I still want to make some major revisions, so I wouldn't recommend to use it for anything serious.

Installation
-------------------------------------------------------------------------------
Java 8 is required to run the app.

Download the [Jar archive](https://www.dropbox.com/s/txvvyesocgxvsmz/sestim-0.1.0.1.zip?dl=0), unpack it anywhere you like and just run it as any other Jar file.

Although it was tested only on Ubuntu and Windows, it should also work on any other OS that has a JVM installed.
