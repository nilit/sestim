package io.gitlab.zalent.sestim.database.entity

import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.utils.Gen
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
@Transactional
class TaskTest {
    @Autowired private lateinit var gen: Gen
    
    @Test
    fun `tasks names can be non-unique when in different stories`() {
        val taskName = "tt"
        val project = gen.project()
        
        val story1 = gen.story("st1", project = project)
        gen.task(name = taskName, story = story1)
        
        val story2 = gen.story("st2", project = project)
        gen.task(name = taskName, story = story2)
    }
    
    @Test(expected = DataIntegrityViolationException::class)
    fun `task names cannot be non-unique in the same story`() {
        val story = gen.story()
        val taskName = "tt"
        gen.task(name = taskName, story = story)
        gen.task(name = taskName, story = story)
    }
}
