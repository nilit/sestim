package io.gitlab.zalent.sestim.database

import org.junit.Test
import kotlin.test.assertEquals

class TimeTest {
    @Test
    fun `hours increment works with zero initial value`() {
        val time = Time()
        time.hours += 4
        assertEquals(Time(hours = 4, minutes = 0), time)
    }
    
    @Test
    fun `hours increment works with non-zero initial value`() {
        val time = Time(hours = 10)
        time.hours += 4
        assertEquals(Time(hours = 14, minutes = 0), time)
    }
    
    @Test
    fun `minutes increment works well with minutes`() {
        val time = Time(hours = 10, minutes = 58)
        time.hours += 4
        assertEquals(Time(hours = 14, minutes = 58), time)
    }
    
    @Test
    fun `minutes increment works with zero initial value`() {
        val time = Time()
        time.minutes += 4
        assertEquals(Time(hours = 0, minutes = 4), time)
    }
    
    @Test
    fun `minutes increment works with non-zero initial value`() {
        val time = Time(minutes = 38)
        time.minutes += 4
        assertEquals(Time(hours = 0, minutes = 42), time)
    }
    
    @Test
    fun `minutes increment works well with hours`() {
        val time = Time(hours = 5, minutes = 38)
        time.minutes += 4
        assertEquals(Time(hours = 5, minutes = 42), time)
    }
    
    @Test
    fun `minutes increment increments hours when required`() {
        val time = Time(minutes = 58)
        time.minutes += 4
        assertEquals(Time(hours = 1, minutes = 2), time)
    }
    
    @Test
    fun `minutes decrement decrements hours when required`() {
        val time = Time(hours = 1, minutes = 2)
        time.minutes -= 4
        assertEquals(Time(hours = 0, minutes = 58), time)
    }
    
    private fun assertEquals(timeExpected: Time, timeActual: Time) {
        assertEquals(timeExpected.toString(), timeActual.toString())
    }
}
