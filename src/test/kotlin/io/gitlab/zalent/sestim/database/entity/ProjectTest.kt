
import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.database.repository.ProjectRepository
import io.gitlab.zalent.sestim.utils.Gen
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.transaction.Transactional
import kotlin.test.assertEquals

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
@Transactional
class ProjectTest {
    @Autowired private lateinit var gen: Gen
    @Autowired private lateinit var projectRepo: ProjectRepository
    
    @Test
    fun `create`() {
        val project = gen.project()
        val projectNameNew = "new name"
        project.name = projectNameNew
        project.color = Integer.parseInt("fff", 16)
        projectRepo.save(project)
        
        val projectUpdated = projectRepo.findOne(project.id)
        assertEquals(projectNameNew, projectUpdated.name)
        assertEquals(project.state, projectUpdated.state)
        assertEquals(project.color, projectUpdated.color)
    }

    @Test
    fun `delete`() {
        val project = gen.project()
        assertEquals(1, projectRepo.count())
        projectRepo.delete(project)
        assertEquals(0, projectRepo.count())
    }
}
