package io.gitlab.zalent.sestim.database.entity

import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.database.repository.ProjectRepository
import io.gitlab.zalent.sestim.database.repository.ProjectTypeRepository
import io.gitlab.zalent.sestim.utils.Gen
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.transaction.Transactional
import kotlin.test.assertEquals

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
@Transactional
class ProjectTypeTest {
    @Autowired private lateinit var gen: Gen
    @Autowired private lateinit var projectRepo: ProjectRepository
    @Autowired private lateinit var projectTypeRepo: ProjectTypeRepository

    @Test
    fun `create`() {
        val projectType = gen.projectType()
        val project = gen.project()
        project.type = projectType
        projectRepo.save(project)
        
        val projectCreated = projectRepo.findOne(project.id)
        assertEquals(projectType.name, projectCreated.type!!.name)
    }
    
    @Test
    fun `edit`() {
        val projectType = gen.projectType()
        val nameNew = "name new"
        projectType.name = nameNew
        projectTypeRepo.save(projectType)
        
        val projectTypeEdited = projectTypeRepo.findOne(projectType.id)
        assertEquals(nameNew, projectTypeEdited.name)
    }
}
