
import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.database.repository.StoryRepository
import io.gitlab.zalent.sestim.utils.Gen
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.transaction.Transactional
import kotlin.test.assertEquals

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
@Transactional
class StoryTest {
    @Autowired private lateinit var storyRepo: StoryRepository
    @Autowired private lateinit var gen: Gen

    @Test
    fun `delete a story`() {
        val story = gen.story()
        assertEquals(storyRepo.count(), 1)
        storyRepo.delete(story)
        assertEquals(storyRepo.count(), 0)
    }
    
    @Test
    fun `story names can be non-unique when in different projects`() {
        val storyName = "st"
        
        val project1 = gen.project("pt1")
        gen.story(name = storyName, project = project1)
        
        val project2 = gen.project("pt2")
        gen.story(name = storyName, project = project2)
    }
    
    @Test(expected = DataIntegrityViolationException::class)
    fun `story names cannot be non-unique in the same project`() {
        val project = gen.project()
        val storyName = "st"
        gen.story(name = storyName, project = project)
        gen.story(name = storyName, project = project)
    }
}
