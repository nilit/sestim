package io.gitlab.zalent.sestim.gui.view.project

import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.repository.ProjectRepository
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.testfx.RollbackIntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.queries.assertPresent
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.robot.robot
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

// TODO move out the fields' tests
class ProjectGuiTest : RollbackIntegrationTestCase() {
    @Autowired private lateinit var projectRepo: ProjectRepository

    @Test
    fun `tableview correctly adjusts its height based on the num of projects`() {
        robot.project.create("1")
        robot.project.create("2")
        assertPresent("1", cssClass = css.cls.tableCell)
        assertPresent("2", cssClass = css.cls.tableCell)
    }

    @Test
    fun `create, edit, delete`() {
        robot.project.create()
        val nameNew = "ptn"
        robot.project.edit(nameNew = nameNew)
        robot.project.delete(nameNew)
    }

    @Test
    fun `KLOC field manipulation`() {
        val projectKLOC = "423"
        robot.project.create(KLOC = projectKLOC)
        assertKLOCPresent(projectKLOC)

        val projectKLOCNew = "821"
        robot.project.edit(KLOCNew = projectKLOCNew)
        assertKLOCPresent(projectKLOCNew)
    }

    @Test
    fun `on a project delete also removes all its stories with their estimates`() {
        robot.`create project with story`()
        robot.project.openEditForm()
        robot.story.openEditForm()
        robot.estimate.create(best = Time(hours = 5))
        robot.`save story and return to ProjectList`()
        robot.project.delete()
    }

    @Test
    fun `dateClosed is set on a project's archiving`() {
        robot.project.create()
        val project = projectRepo.findAll().first()
        assertEquals(null, project.dateClosed)
        val dateClosedOriginal = project.dateClosed.toString()

        robot.project.edit(stateNew = Project.State.Archived)
        val dateClosed = projectRepo.findAll().first().dateClosed.toString()
        assertNotEquals(dateClosedOriginal, dateClosed)
    }

    private fun assertKLOCPresent(KLOC: String) {
        robot.project.openEditForm()
        assertPresent(KLOC, parentSelector = id[ident.project.edit.field.KLOC])
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.project.list.node])
    }
}
