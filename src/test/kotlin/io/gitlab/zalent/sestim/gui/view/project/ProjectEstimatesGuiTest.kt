package io.gitlab.zalent.sestim.gui.view.project

import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.testfx.RollbackIntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.queries.assertCheckboxNotSelected
import io.gitlab.zalent.sestim.utils.testfx.queries.assertCheckboxSelected
import io.gitlab.zalent.sestim.utils.testfx.queries.assertNotPresent
import io.gitlab.zalent.sestim.utils.testfx.queries.assertPresent
import io.gitlab.zalent.sestim.utils.testfx.robot.robot
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import org.junit.Test

class ProjectEstimatesGuiTest : RollbackIntegrationTestCase() {
    @Test
    fun `estimate list is a fragment that can be present at several places`() {
        robot.`create project with story`()
        
        robot.project.openEditForm()
        assertPresent(id[ident.estimate.list.node])
        robot.story.openEditForm()
        assertPresent(id[ident.estimate.list.node])
        
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.project.edit.form])
        assertPresent(id[ident.estimate.list.node])
    }
    
    @Test
    fun `create, delete`() {
        robot.project.create()
        robot.project.openEditForm()
        val bestTime = Time(5)
        robot.estimate.create(bestTime)
        assertPresent(bestTime.toString(), cssClass = css.cls.tableCell)
        robot.estimate.delete(rowIndex = 0)
        assertNotPresent(bestTime.toString(), cssClass = css.cls.tableCell)
    }
    
    @Test
    fun `estimate edit view can edit an estimate`() {
        robot.project.create()
        robot.project.openEditForm()
        robot.estimate.create()
        val bestValueNew = Time(9)
        robot.estimate.edit(rowIndex = 0, bestNew = bestValueNew)
        clickOn(id[ident.header.button.refresh])
        waitForNode(bestValueNew.toString(), cssClass = css.cls.tableCell)
    }

    @Test
    fun `an estimate calc includes stories and their tasks`() {
        robot.project.create()
        robot.project.openEditForm()
        
        robot.story.create()
        robot.story.openEditForm()
        val storyEstimate = Estimate(bestHours = 10, worstHours = 23, expectedHours = 17, actualHours = 7)
        robot.estimate.create(storyEstimate)
        
        robot.task.create()
        robot.task.openEditForm()
        val taskEstimate = Estimate(bestHours = 5, worstHours = 12, expectedHours = 8, actualHours = 3)
        robot.estimate.create(taskEstimate)
        robot.task.save()
        
        robot.story.save()
        
        robot.estimate.create()
        robot.estimate.openEditForm()
        
        val estimateCalcExpected = Estimate.Calc()
        estimateCalcExpected += storyEstimate
        estimateCalcExpected += taskEstimate
        
        clickOn(id[ident.estimate.calcList.button.calculate])
        waitForNode(estimateCalcExpected.best.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.worst.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.expected.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.actual.toString(), cssClass = css.cls.tableCell)
    }
    
    @Test
    fun `an estimate calc includes stories and their tasks (excluding not active ones)`() {
        robot.project.create()
        robot.project.openEditForm()
        
        
        robot.story.create()
        robot.story.openEditForm()
        val storyActiveEstimate = Estimate(bestHours = 10, worstHours = 23, expectedHours = 17, actualHours = 7)
        robot.estimate.create(storyActiveEstimate)
        
        robot.task.create()
        robot.task.openEditForm()
        val taskActiveEstimate = Estimate(bestHours = 5, worstHours = 12, expectedHours = 8, actualHours = 3)
        robot.estimate.create(taskActiveEstimate)
        robot.task.save()
        
        val taskNameNotActive = "task not active"
        robot.task.create(taskNameNotActive, state = PlanItemEntity.State.Completed)
        robot.task.openEditForm(taskNameNotActive)
        val taskNotActiveEstimate = Estimate(bestHours = 5, worstHours = 12, expectedHours = 8, actualHours = 3)
        robot.estimate.create(taskNotActiveEstimate)
        robot.task.save()
        
        robot.story.save()
        
        
        val storyNameNotActive = "story not active"
        robot.story.create(name = storyNameNotActive, state = PlanItemEntity.State.Completed)
        robot.story.openEditForm(storyNameNotActive)
        val storyNotActiveEstimate = Estimate(bestHours = 10, worstHours = 23, expectedHours = 17, actualHours = 7)
        robot.estimate.create(storyNotActiveEstimate)
        robot.story.save()
        
        
        robot.estimate.create()
        robot.estimate.openEditForm()
        
        val estimateCalcExpected = Estimate.Calc()
        estimateCalcExpected += storyActiveEstimate
        estimateCalcExpected += taskActiveEstimate
        
        clickOn(id[ident.estimate.calcList.button.calculate])
        waitForNode(estimateCalcExpected.best.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.worst.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.expected.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.actual.toString(), cssClass = css.cls.tableCell)
    }
    
    @Test
    fun `estimate calc list is a Fragment and can be present at several parents`() {
        robot.project.create()
        robot.project.openEditForm()
        
        robot.estimate.create()
        robot.estimate.openEditForm()
        assertPresent(id[ident.estimate.calcList.node])
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.project.edit.form])
        
        robot.story.create(state = PlanItemEntity.State.Completed)
        robot.story.openEditForm()
        val storyEstimate = Estimate(bestHours = 10, worstHours = 23, expectedHours = 17, actualHours = 7)
        robot.estimate.create(storyEstimate)
        robot.estimate.openEditForm()
        assertPresent(id[ident.estimate.calcList.node])
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.story.edit.form])
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.project.edit.form])
        
        robot.estimate.openEditForm()
        assertPresent(id[ident.estimate.calcList.node])
    }
    
    @Test
    fun `the field 'isDateValid' works`() {
        robot.project.create()
        robot.project.openEditForm()
        
        robot.estimate.create(isDateValid = false)
        
        robot.estimate.openEditForm()
        assertCheckboxNotSelected(ident.estimate.edit.field.isDateValid)
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.project.edit.form])
        
        robot.estimate.edit(isDateValid = true)
        robot.estimate.openEditForm()
        assertCheckboxSelected(ident.estimate.edit.field.isDateValid)
    }
    
    private data class FixtureSet(
        val projectName: String,
        val estimateValue: Int
    )
}
