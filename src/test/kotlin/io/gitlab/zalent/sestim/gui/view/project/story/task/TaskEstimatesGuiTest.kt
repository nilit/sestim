package io.gitlab.zalent.sestim.gui.view.project.story.task

import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.utils.testfx.RollbackIntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.queries.assertNotPresent
import io.gitlab.zalent.sestim.utils.testfx.queries.assertPresent
import io.gitlab.zalent.sestim.utils.testfx.robot.robot
import org.junit.Test

class TaskEstimatesGuiTest : RollbackIntegrationTestCase() {
    // TODO unstable in headless mode
    // can fail in different places
    @Test
    fun `create, delete`() {
        val sets = arrayOf(
            FixtureSet(storyName = "st1", taskName = "tt1", estimateValue = Time(hours = 5)),
            FixtureSet(storyName = "st2", taskName = "tt2", estimateValue = Time(hours = 17))
        )

        robot.project.create()
        robot.project.openEditForm()

        // create sets[0]
        robot.story.create(sets[0].storyName)
        robot.story.openEditForm(sets[0].storyName)
        robot.task.create(sets[0].taskName)
        robot.task.openEditForm(sets[0].taskName)
        robot.estimate.create(sets[0].estimateValue)
        robot.task.save()
        robot.story.save()

        // create sets[1]
        robot.story.create(sets[1].storyName)
        robot.story.openEditForm(sets[1].storyName)
        robot.task.create(sets[1].taskName)
        robot.task.openEditForm(sets[1].taskName)
        robot.estimate.create(sets[1].estimateValue)
        robot.task.save()
        robot.story.save()

        // delete sets[1]'s estimate
        robot.story.openEditForm(sets[1].storyName)
        robot.task.openEditForm(sets[1].taskName)
        robot.estimate.delete(rowIndex = 0)
        robot.task.save()
        robot.story.save()

        // check sets[0]
        robot.story.openEditForm(sets[0].storyName)
        robot.task.openEditForm(sets[0].taskName)
        assertPresent(sets[0].estimateValue.toString(), cssClass = css.cls.tableCell)
        robot.task.save()
        robot.story.save()

        // check sets[1]
        robot.story.openEditForm(sets[1].storyName)
        robot.task.openEditForm(sets[1].taskName)
        assertNotPresent(sets[1].estimateValue.toString(), cssClass = css.cls.tableCell)
        robot.task.save()
        robot.story.save()
    }

    private data class FixtureSet(
        val storyName: String,
        val taskName: String,
        val estimateValue: Time
    )
}
