package io.gitlab.zalent.sestim.gui.view.project.story

import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.testfx.RollbackIntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.queries.assertNotPresent
import io.gitlab.zalent.sestim.utils.testfx.queries.assertPresent
import io.gitlab.zalent.sestim.utils.testfx.robot.robot
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import org.junit.Test

class StoryEstimatesGuiTest : RollbackIntegrationTestCase() {
    /**
     * I use here two sets of objects to ensure that I cover the bug with keys
     * pressing.
     * For detail see the [issue](https://github.com/TestFX/TestFX/issues/449).
     */
    @Test
    fun `create, edit, delete`() {
        val sets = arrayOf(
            FixtureSet(projectName = "pt1", storyName = "st1", estimateValue = Time(hours = 5)),
            FixtureSet(projectName = "pt2", storyName = "st2", estimateValue = Time(hours = 17))
        )

        // create sets[0]
        robot.`create project with story`(sets[0].projectName, sets[0].storyName)
        robot.project.openEditForm(sets[0].projectName)
        robot.story.openEditForm(sets[0].storyName)
        robot.estimate.create(sets[0].estimateValue)
        assertPresent(sets[0].estimateValue.toString(), cssClass = css.cls.tableCell)
        robot.`save story and return to ProjectList`()

        // create sets[1]
        robot.`create project with story`(sets[1].projectName, sets[1].storyName)
        robot.project.openEditForm(sets[1].projectName)
        robot.story.openEditForm(sets[1].storyName)
        robot.estimate.create(sets[1].estimateValue)
        assertPresent(sets[1].estimateValue.toString(), cssClass = css.cls.tableCell)
        robot.`save story and return to ProjectList`()

        // change the sets[0]'s estimate value to `bestValueNew`
        robot.project.openEditForm(sets[0].projectName)
        robot.story.openEditForm(sets[0].storyName)
        val bestValueNew = Time(hours = 3)
        robot.estimate.edit(rowIndex = 0, bestNew = bestValueNew)
        assertPresent(bestValueNew.toString(), cssClass = css.cls.tableCell)
        robot.`save story and return to ProjectList`()

        // delete the sets[1]'s estimate
        robot.project.openEditForm(sets[1].projectName)
        robot.story.openEditForm(sets[1].storyName)
        val estimateToDeleteRowIndex = 0
        robot.estimate.delete(rowIndex = estimateToDeleteRowIndex)
        robot.`save story and return to ProjectList`()

        // assert that the sets[0]'s estimate is still fine
        robot.project.openEditForm(sets[0].projectName)
        robot.story.openEditForm(sets[0].storyName)
        assertPresent(bestValueNew.toString(), cssClass = css.cls.tableCell)
        robot.`save story and return to ProjectList`()

        // assert that the sets[1]'s estimate was deleted
        robot.project.openEditForm(sets[1].projectName)
        robot.story.openEditForm(sets[1].storyName)
        val estimateDeleted = sets[1].estimateValue
        assertNotPresent(estimateDeleted.toString(), cssClass = css.cls.tableCell)
        robot.`save story and return to ProjectList`()
    }

    @Test
    fun `an estimate calc is correctly based on tasks (excluding not active ones)`() {
        robot.project.create()
        robot.project.openEditForm()

        robot.story.create()
        robot.story.openEditForm()


        val taskNameActive1 = "task active 1"
        robot.task.create(taskNameActive1)
        robot.task.openEditForm(taskNameActive1)
        val taskActiveEstimate1 = Estimate(
            bestHours = 25, worstHours = 12, expectedHours = 8, actualHours = 3
        )
        robot.estimate.create(taskActiveEstimate1)
        robot.task.save()

        val taskNameNotActive = "task not active"
        robot.task.create(taskNameNotActive, state = PlanItemEntity.State.Completed)
        robot.task.openEditForm(taskNameNotActive)
        val taskNotActiveEstimate = Estimate(
            bestHours = 3, worstHours = 12, expectedHours = 8, actualHours = 3
        )
        robot.estimate.create(taskNotActiveEstimate)
        robot.task.save()

        val taskNameActive2 = "task active 2"
        robot.task.create(taskNameActive2)
        robot.task.openEditForm(taskNameActive2)
        val taskActiveEstimate2 = Estimate(
            bestHours = 99, worstHours = 12, expectedHours = 8, actualHours = 3
        )
        robot.estimate.create(taskActiveEstimate2)
        robot.task.save()


        robot.story.save()

        robot.estimate.create()
        robot.estimate.openEditForm()

        val estimateCalcExpected = Estimate.Calc()
        estimateCalcExpected += taskActiveEstimate1
        estimateCalcExpected += taskActiveEstimate2

        clickOn(id[ident.estimate.calcList.button.calculate])
        waitForNode(estimateCalcExpected.best.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.worst.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.expected.toString(), cssClass = css.cls.tableCell)
        assertPresent(estimateCalcExpected.actual.toString(), cssClass = css.cls.tableCell)
    }

    private data class FixtureSet(
        val projectName: String,
        val storyName: String,
        val estimateValue: Time
    )
}
