package io.gitlab.zalent.sestim.gui.view.project.story

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.database.repository.StoryRepository
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.default
import io.gitlab.zalent.sestim.utils.testfx.RollbackIntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.queries.assertNotPresent
import io.gitlab.zalent.sestim.utils.testfx.queries.assertPresent
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.queries.find
import io.gitlab.zalent.sestim.utils.testfx.robot.robot
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class StoryGuiTest : RollbackIntegrationTestCase() {
    @Autowired private lateinit var storyRepo: StoryRepository
    
    @Test
    fun `create, edit, delete`() {
        robot.project.create()
        robot.project.openEditForm()
        
        robot.story.create(state = PlanItemEntity.State.Completed)
        assertPresent(default.story.name, css.cls.tableCell)

        val storyNameNew = "tsn"
        robot.story.edit(
            nameOld = default.story.name,
            nameNew = storyNameNew,
            typeNew = PlanItemEntity.Type.Bug
        )
        robot.story.delete(storyNameNew)
        assertNotPresent(storyNameNew, css.cls.tableCell)
    }
    
    @Test
    fun `move a story to another project`() {
        val projectSourceName = "pt1"
        robot.`create project with story`(projectSourceName, default.story.name)

        val projectTargetName = "pt2"
        robot.project.create(projectTargetName)
        
        `move story to another project`(default.story.name, projectSourceName, projectTargetName)
        
        robot.project.openEditForm(projectSourceName)
        assertNotPresent(default.story.name, css.cls.tableCell)
        clickOn(id[ident.header.button.back])
        
        clickOn(projectTargetName)
        assertPresent(default.story.name, cssClass = css.cls.tableCell)
    }
    
    @Test
    fun `story create view has the selected project on init`() {
        robot.project.create()
        openStoryCreateView()
        assertPresent(default.project.name, parentSelector = id[ident.story.edit.field.project])
    }
    
    @Test
    fun `story edit view contains all the projects in its project field`() {
        robot.`create project with story`()
        
        val projectSecondName = "tp2"
        robot.project.create(projectSecondName)
        val projectThirdName = "tp3"
        robot.project.create(projectThirdName)
        
        robot.project.openEditForm()
        robot.story.openEditForm()
        clickOn(id[ident.story.edit.field.project])
        assertPresent(default.project.name, parentCssClass = css.cls.contextMenu)
        assertPresent(projectSecondName, parentCssClass = css.cls.contextMenu)
        assertPresent(projectThirdName, parentCssClass = css.cls.contextMenu)
    }

    @Test
    fun `storyModel rebinds itself on StoryEditView's dock`() {
        robot.`create project with story`()

        robot.project.openEditForm()
        robot.story.openEditForm()
        robot.story.save()
        robot.project.save()

        robot.project.openEditForm()
        robot.story.openEditForm()
        robot.story.save()
        robot.project.save()
    }

    @Test
    fun `dateClosed is set on a story's completing`() {
        robot.`create project with story`()
        robot.project.openEditForm()
        
        val story = storyRepo.findAll().first()
        assertEquals(null, story.dateClosed)
        val dateClosedOriginal = story.dateClosed.toString()
        
        robot.story.edit(stateNew = PlanItemEntity.State.Completed)
        val dateClosed = storyRepo.findAll().first().dateClosed.toString()
        assertNotEquals(dateClosedOriginal, dateClosed)
    }
    
    private fun `move story to another project`(
        storyName: String, projectSourceName: String, projectTargetName: String
    ) {
        robot.project.openEditForm(projectSourceName)
        robot.story.openEditForm(storyName)
        
        clickOn(id[ident.story.edit.field.project])
        clickOn(projectTargetName)
        
        clickOn(id[ident.header.button.save])
        waitForNode(id[ident.story.list.node])
        
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.project.list.node])
    }
    
    private fun openStoryCreateView(projectName: String = default.project.name) {
        clickOn(find(projectName, css.cls.tableCell))
        clickOn(id[ident.story.list.button.add])
        waitForNode(id[ident.story.edit.form])
    }
}
