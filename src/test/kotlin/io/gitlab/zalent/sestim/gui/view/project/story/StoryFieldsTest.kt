package io.gitlab.zalent.sestim.gui.view.project.story

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.testfx.RollbackIntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.queries.assertNotPresent
import io.gitlab.zalent.sestim.utils.testfx.queries.assertPresent
import io.gitlab.zalent.sestim.utils.testfx.queries.find
import io.gitlab.zalent.sestim.utils.testfx.robot.robot
import org.junit.Test
import kotlin.test.assertTrue

class StoryFieldsTest : RollbackIntegrationTestCase() {
    @Test
    fun `the field 'name' doesn't allow to save a story when empty`() {
        robot.`create project with story`()
        
        robot.project.openEditForm()
        robot.story.openEditForm()

        robot.writer.clearField(id[ident.story.edit.field.name])
        assertTrue(find(id[ident.header.button.save])!!.isDisabled)
    }
    
    @Test
    fun `the field 'size' works`() {
        robot.`create project with story`()
        robot.project.openEditForm()
        
        val sizeNew = PlanItemEntity.Size.VeryLarge
        robot.story.edit(sizeNew = sizeNew)
        robot.story.openEditForm()
        assertPresent(sizeNew.name)
    }
    
    @Test
    fun `the field 'LOC' works`() {
        robot.`create project with story`()
        robot.project.openEditForm()
        
        val LOCNew = "643"
        robot.story.edit(LOCNew = LOCNew)
        robot.story.openEditForm()
        assertPresent(LOCNew)
    }
    
    @Test
    fun `the field 'Type' can be unset`() {
        robot.`create project with story`()
        robot.project.openEditForm()
        
        robot.story.edit(typeNew = PlanItemEntity.Type.NotSet)
        
        robot.story.openEditForm()
        assertNotPresent(PlanItemEntity.Type.Feature.toString())
        assertPresent(PlanItemEntity.Type.NotSet.toString())
    }
}
