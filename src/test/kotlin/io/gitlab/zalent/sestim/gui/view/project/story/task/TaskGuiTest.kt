package io.gitlab.zalent.sestim.gui.view.project.story.task

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.database.repository.TaskRepository
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.default
import io.gitlab.zalent.sestim.utils.testfx.RollbackIntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.queries.assertNotPresent
import io.gitlab.zalent.sestim.utils.testfx.queries.assertPresent
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.robot.robot
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class TaskGuiTest : RollbackIntegrationTestCase() {
    @Autowired private lateinit var taskRepo: TaskRepository
    
    @Test
    fun `create, edit, delete`() {
        robot.`create project with story`()

        robot.project.openEditForm()
        robot.story.openEditForm()
        robot.task.create()

        val taskNameNew = "ttn"
        robot.task.edit(nameOld = default.task.name, nameNew = taskNameNew)
        assertPresent(taskNameNew, css.cls.tableCell)

        robot.task.delete(taskNameNew)
        assertNotPresent(taskNameNew, css.cls.tableCell)
    }
    
    @Test
    fun `move a task to another story`() {
        robot.project.create()
        robot.project.openEditForm()
        
        val sourceStoryName = "st1"
        robot.story.create(sourceStoryName)
        val targetStoryName = "st2"
        robot.story.create(targetStoryName)

        robot.story.openEditForm(sourceStoryName)
        val taskName = "tt"
        robot.task.create(taskName)

        `move task to another story`(taskName, sourceStoryName, targetStoryName)
    
        robot.story.openEditForm(sourceStoryName)
        assertNotPresent(taskName, css.cls.tableCell)
        clickOn(id[ident.header.button.back])
        waitForNode(id[ident.story.list.node])
    
        robot.story.openEditForm(targetStoryName)
        assertPresent(taskName, css.cls.tableCell)
    }
    
    @Test
    fun `the field 'size' works`() {
        robot.`create project with story and task`()
        robot.project.openEditForm()
        robot.story.openEditForm()
        
        val sizeNew = PlanItemEntity.Size.Medium
        robot.task.edit(sizeNew = sizeNew)
        robot.task.openEditForm()
        assertPresent(sizeNew.name)
    }
    
    @Test
    fun `the field 'LOC' works`() {
        robot.`create project with story and task`()
        robot.project.openEditForm()
        robot.story.openEditForm()
        
        val LOCNew = "642"
        robot.task.edit(LOCNew = LOCNew)
        robot.task.openEditForm()
        assertPresent(LOCNew)
    }
    
    @Test
    fun `dateClosed is set on a story's completing`() {
        robot.`create project with story and task`()
        robot.project.openEditForm()
        robot.story.openEditForm()

        val task = taskRepo.findAll().first()
        assertEquals(null, task.dateClosed)
        val dateClosedOriginal = task.dateClosed.toString()

        robot.task.edit(stateNew = PlanItemEntity.State.Completed)
        val dateClosed = taskRepo.findAll().first().dateClosed.toString()
        assertNotEquals(dateClosedOriginal, dateClosed)
    }
    
    private fun `move task to another story`(
        taskName: String, sourceStoryName: String, targetStoryName: String
    ) {
        robot.task.openEditForm(taskName)
        
        clickOn(id[ident.task.edit.field.story])
        clickOn(targetStoryName)
        
        clickOn(id[ident.header.button.save])
        waitForNode(id[ident.story.edit.form])
        
        assertNotPresent(taskName, cssClass = css.cls.tableCell)
        
        clickOn(id[ident.header.button.save])
        waitForNode(id[ident.story.list.node])
    }
}
