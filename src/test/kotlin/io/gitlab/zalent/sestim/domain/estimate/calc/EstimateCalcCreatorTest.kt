package io.gitlab.zalent.sestim.domain.estimate.calc

import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.service.EstimateCalcService
import io.gitlab.zalent.sestim.utils.Gen
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
@Transactional
class EstimateCalcCreatorTest {
    @Autowired private lateinit var gen: Gen
    @Autowired private lateinit var calcCreator: EstimateCalcCreator
    @Autowired private lateinit var calcService: EstimateCalcService
    
    @Test
    fun `for a project creates an estimate calc that includes its stories and their tasks`() {
        val project = gen.project()
        
        val storyEstimate = gen.estimate(best = 7, worst = 22, expected = 15)
        val story = gen.story(project = project, estimates = listOf(storyEstimate))
        
        val taskEstimate = gen.estimate(best = 3, worst = 11, expected = 8)
        gen.task(story = story, estimates = listOf(taskEstimate))
        
        val projectEstimate = gen.estimate(project = project)
        calcCreator.create(projectEstimate, project)
        val estimateCalcActual: Estimate.Calc = calcService.getByEstimate(projectEstimate).first()
        
        val estimateCalcExpected = Estimate.Calc()
        estimateCalcExpected += storyEstimate
        estimateCalcExpected += taskEstimate
        
        assertEstimatesEqual(estimateCalcExpected, estimateCalcActual)
    }
}
