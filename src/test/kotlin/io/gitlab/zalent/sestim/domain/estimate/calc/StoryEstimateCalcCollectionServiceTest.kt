package io.gitlab.zalent.sestim.domain.estimate.calc

import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.utils.Gen
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
@Transactional
class StoryEstimateCalcCollectionServiceTest {
    @Autowired private lateinit var gen: Gen
    @Autowired private lateinit var calcService: EstimateCalcCollectionService

    private val estimateZero = Estimate()

    @Test
    fun `doesn't calculate completed tasks`() {
        val story = gen.story()

        val estimateValid = gen.estimate(best = 4, worst = 8, expected = 4)
        gen.task(story = story, estimates = listOf(estimateValid), state = PlanItemEntity.State.Completed)

        val estimateCalculated: Estimate.Calc = calcService.collect(story)
        assertEstimatesEqual(estimateZero, estimateCalculated)
    }

    @Test
    fun `calculates only non-completed tasks`() {
        val story = gen.story()

        gen.task(
            state = PlanItemEntity.State.Completed,
            story = story,
            estimates = listOf(gen.estimate(best = 4, worst = 10, expected = 5, actual = 8))
        )

        val estimateValid = gen.estimate(best = 3, worst = 11, expected = 8)
        gen.task(
            name = "task in dev",
            story = story,
            estimates = listOf(estimateValid),
            state = PlanItemEntity.State.InDev
        )

        val estimateCalculated: Estimate.Calc = calcService.collect(story)
        assertEstimatesEqual(estimateValid, estimateCalculated)
    }

    @Test
    fun `calculates only the last valid estimate v1`() {
        val estimates = object {
            val valid = gen.estimate(best = 4, worst = 10, expected = 5)
            val invalidLast = gen.estimate(worst = 8, expected = 4)
        }
        `assert calculates only the given estimate`(
            estimates = listOf(estimates.valid, estimates.invalidLast),
            estimateExpected = estimates.valid
        )
    }

    @Test
    fun `calculates only the last valid estimate v2`() {
        val estimates = object {
            val valid = gen.estimate(best = 3, worst = 11, expected = 8)
            val validLast = gen.estimate(best = 4, worst = 10, expected = 5, actual = 8)
        }
        `assert calculates only the given estimate`(
            estimates = listOf(estimates.valid, estimates.validLast),
            estimateExpected = estimates.validLast
        )
    }

    private fun `assert calculates only the given estimate`(
        estimates: List<Estimate>, estimateExpected: Estimate
    ) {
        val story = gen.story()
        gen.task(story = story, estimates = estimates)
        val estimateCalculated: Estimate.Calc = calcService.collect(story)
        assertEstimatesEqual(estimateExpected, estimateCalculated)
    }
}
