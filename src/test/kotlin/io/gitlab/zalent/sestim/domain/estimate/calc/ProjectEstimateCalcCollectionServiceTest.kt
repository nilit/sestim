package io.gitlab.zalent.sestim.domain.estimate.calc

import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.utils.Gen
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
@Transactional
class ProjectEstimateCalcCollectionServiceTest {
    @Autowired private lateinit var gen: Gen
    @Autowired private lateinit var calcCollectionService: EstimateCalcCollectionService

    private val estimateZero = Estimate.Calc()

    @Test
    fun `doesn't calculate completed tasks of its stories`() {
        val project = gen.project()
        
        val story = gen.story(project = project)
        val taskEstimate = gen.estimate(best = 4, worst = 8, expected = 4)
        gen.task(story = story, estimates = listOf(taskEstimate), state = PlanItemEntity.State.Completed)

        val estimateCalculated: Estimate.Calc = calcCollectionService.collect(project)
        assertEstimatesEqual(estimateZero, estimateCalculated)
    }

    @Test
    fun `calculates only non-completed tasks`() {
        val project = gen.project()
        
        val story = gen.story(project = project)
        gen.task(
            state = PlanItemEntity.State.Completed,
            story = story,
            estimates = listOf(gen.estimate(best = 4, worst = 10, expected = 5, actual = 8))
        )
        val estimateValid = gen.estimate(best = 3, worst = 11, expected = 8)
        gen.task(
            name = "task in dev",
            story = story,
            estimates = listOf(estimateValid),
            state = PlanItemEntity.State.InDev
        )

        val estimateCalculated: Estimate.Calc = calcCollectionService.collect(project)
        assertEstimatesEqual(estimateValid, estimateCalculated)
    }
    
    @Test
    fun `calculates only non-completed stories and their tasks`() {
        val project = gen.project()
        
        val storyNonCompleted = gen.story(project = project)
        gen.task(
            state = PlanItemEntity.State.Completed,
            story = storyNonCompleted,
            estimates = listOf(gen.estimate(best = 4, worst = 10, expected = 5, actual = 8))
        )
        val estimateValid = gen.estimate(best = 3, worst = 11, expected = 8)
        gen.task(
            name = "task in dev",
            story = storyNonCompleted,
            estimates = listOf(estimateValid),
            state = PlanItemEntity.State.InDev
        )

        gen.story(
            name = "story completed",
            project = project,
            estimates = listOf(gen.estimate(best = 4, worst = 10, expected = 5, actual = 8)),
            state = PlanItemEntity.State.Completed
        )

        val estimateCalculated: Estimate.Calc = calcCollectionService.collect(project)
        assertEstimatesEqual(estimateValid, estimateCalculated)
    }

    @Test
    fun `calculates only the last valid estimate v1`() {
        val estimates = object {
            val valid = gen.estimate(best = 4, worst = 10, expected = 5)
            val invalidLast = gen.estimate(worst = 8, expected = 4)
        }
        `assert calculates only the given estimate`(
            projectEstimates = listOf(estimates.valid, estimates.invalidLast),
            estimateExpected = estimates.valid
        )
    }

    @Test
    fun `calculates only the last valid estimate v2`() {
        val estimates = object {
            val valid = gen.estimate(best = 3, worst = 11, expected = 8)
            val validLast = gen.estimate(best = 4, worst = 10, expected = 5, actual = 8)
        }
        `assert calculates only the given estimate`(
            projectEstimates = listOf(estimates.valid, estimates.validLast),
            estimateExpected = estimates.validLast
        )
    }

    private fun `assert calculates only the given estimate`(
        projectEstimates: List<Estimate>, estimateExpected: Estimate
    ) {
        val project = gen.project()
        gen.story(project = project, estimates = projectEstimates)
        val estimateCalculated: Estimate.Calc = calcCollectionService.collect(project)
        assertEstimatesEqual(estimateExpected, estimateCalculated)
    }
}
