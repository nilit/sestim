package io.gitlab.zalent.sestim.domain.estimate.calc

import io.gitlab.zalent.sestim.database.entity.base.EstimableEntity
import kotlin.test.assertEquals

fun assertEstimatesEqual(estimateExpected: EstimableEntity, estimateActual: EstimableEntity) {
    assertEquals(estimateExpected.best, estimateActual.best)
    assertEquals(estimateExpected.worst, estimateActual.worst)
    assertEquals(estimateExpected.expected, estimateActual.expected)
    assertEquals(estimateExpected.actual, estimateActual.actual)
}
