package io.gitlab.zalent.sestim.utils.testfx.robot

import io.gitlab.zalent.sestim.utils.testfx.IntegrationTestCase
import io.gitlab.zalent.sestim.utils.testfx.queries.find
import javafx.scene.control.Control
import org.testfx.api.FxRobot
import org.testfx.util.WaitForAsyncUtils.asyncFx

object ContextMenuRobot : FxRobot() {
    fun openMenu(
        itemToSelect: String,
        menuOwnerSelector: String, 
        menuSelector: String
    ) {
        `select item and try to open its menu`(itemSelector = itemToSelect)
        if (isContextMenuNotOpened(menuSelector)) {
            forceContextMenuOpen(menuOwnerSelector)
        }
    }
    
    private fun `select item and try to open its menu`(itemSelector: String) {
        rightClickOn(itemSelector)
    }
    
    private fun isContextMenuNotOpened(menuSelector: String) = (find(menuSelector) == null)
    
    /**
     * Because context menu doesn't work with Monocle and it wasn't fixed for
     * more than a year now.
     * For detail see the [issue](https://github.com/TestFX/Monocle/issues/12).
     */
    private fun forceContextMenuOpen(menuOwnerSelector: String) {
        @Suppress("UNCHECKED_CAST")
        val menuOwner = find(menuOwnerSelector)!! as Control
        val contextmenu = menuOwner.contextMenu
        asyncFx { contextmenu.show(IntegrationTestCase.stage.scene.window) }.get()
    }
}
