package io.gitlab.zalent.sestim.utils.testfx.robot

import io.gitlab.zalent.sestim.utils.default
import org.testfx.api.FxRobot

object robot : FxRobot() {
    val project = ProjectRobot
    val story = StoryRobot
    val task = TaskRobot
    val estimate = EstimateRobot
    
    val contextmenu = ContextMenuRobot
    val writer = WriterRobot
    
    fun `create project with story`(
        projectName: String = default.project.name,
        storyName: String = default.story.name
    ) {
        project.create(projectName)
        project.openEditForm(projectName)
        story.create(storyName)
        project.save()
    }
    
    fun `create project with story and task`(
        projectName: String = default.project.name,
        storyName: String = default.story.name,
        taskName: String = default.task.name
    ) {
        project.create(projectName)
        project.openEditForm(projectName)
        story.create(storyName)
        
        story.openEditForm(storyName)
        task.create(taskName)
        
        `save story and return to ProjectList`()
    }

    fun `save story and return to ProjectList`() {
        story.save()
        project.save()
    }
}
