package io.gitlab.zalent.sestim.utils.testfx.queries

import io.gitlab.zalent.sestim.gui.ident.CssId
import io.gitlab.zalent.sestim.utils.testfx.CssClass
import javafx.scene.Node
import org.testfx.api.FxRobot
import org.testfx.service.query.NodeQuery
import tornadofx.*

fun FxRobot.buildQuery(
    selector: String, cssClass: CssClass? = null, cssId: CssId? = null
): NodeQuery {
    var nodeQuery = lookup(selector)
    if (cssClass != null) {
        nodeQuery = nodeQuery.match { node: Node -> node.hasClass(cssClass.name) }
    }
    if (cssId != null) {
        nodeQuery = nodeQuery.match { node: Node -> node.id == cssId.name }
    }
    return nodeQuery
}
