package io.gitlab.zalent.sestim.utils.testfx.robot

import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.gui.ident.cellId
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.EstimateModel
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import org.testfx.api.FxRobot

object EstimateRobot : FxRobot() {
    fun create(estimate: Estimate) {
        create(
            best = estimate.best,
            worst = estimate.worst,
            expected = estimate.expected,
            actual = estimate.actual
        )
    }
    
    fun create(
        best: Time? = null,
        worst: Time? = null,
        expected: Time? = null,
        actual: Time? = null,
        isDateValid: Boolean? = null
    ) {
        clickOn(id[ident.estimate.list.button.add])
        waitForNode(id[ident.estimate.edit.form])
        
        updateFields(
            best = best, worst = worst, expected = expected, actual = actual,
            isDateValid = isDateValid
        )
        
        save()
    }
    
    fun edit(rowIndex: Int = 0, bestNew: Time? = null, isDateValid: Boolean? = null) {
        openEditForm(rowIndex)
        updateFields(best = bestNew, isDateValid = isDateValid)
        save()
    }
    
    fun delete(rowIndex: Int = 0) {
        openContextMenu(rowIndex)
        clickOn(id[ident.estimate.list.contextmenu.delete])
    }

    fun openEditForm(rowIndex: Int = 0) {
        openContextMenu(rowIndex)
        clickOn(id[ident.estimate.list.contextmenu.edit])
        waitForNode(id[ident.estimate.edit.form])
    }

    fun save() {
        clickOn(id[ident.header.button.save])
        waitForNode(id[ident.estimate.list.node])
    }
    
    private fun updateFields(
        best: Time? = null,
        worst: Time? = null,
        expected: Time? = null,
        actual: Time? = null,
        isDateValid: Boolean? = null
    ) {
        if (best != null) {
            robot.writer.writeToTimeField(ident.estimate.edit.field.best, best)
        }
        if (worst != null) {
            robot.writer.writeToTimeField(ident.estimate.edit.field.worst, time = worst)
        }
        if (expected != null) {
            robot.writer.writeToTimeField(ident.estimate.edit.field.expected, time = expected)
        }
        if (actual != null) {
            robot.writer.writeToTimeField(ident.estimate.edit.field.actual, time = actual)
        }
        
        if (isDateValid != null) {
            robot.writer.setCheckbox(ident.estimate.edit.field.isDateValid, isDateValid)
        }
    }
    
    private fun openContextMenu(rowIndex: Int) {
        robot.contextmenu.openMenu(
            itemToSelect = cellId[EstimateModel::best, rowIndex],
            menuOwnerSelector = id[ident.estimate.list.node],
            menuSelector = id[ident.estimate.list.contextmenu.node]
        )
    }
}
