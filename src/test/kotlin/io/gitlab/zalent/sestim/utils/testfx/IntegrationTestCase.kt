package io.gitlab.zalent.sestim.utils.testfx

import io.gitlab.zalent.sestim.SestimApplication
import io.gitlab.zalent.sestim.config.configureHeadlessMode
import javafx.stage.Stage
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.testfx.api.FxToolkit
import org.testfx.framework.junit.ApplicationTest

// TODO context init can happen ahead of javafx init, thus throwing a `Toolkit not initialized`
@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [SestimApplication::class])
abstract class IntegrationTestCase : ApplicationTest() {
    lateinit var app: SestimApplication

    @Autowired private lateinit var context: ConfigurableApplicationContext

    companion object {
        val stage: Stage by lazy { Stage() }

        init {
            configureHeadlessMode()
            `disable the error-prone testfx robot`()
        }

        private fun `disable the error-prone testfx robot`() {
            System.setProperty("testfx.robot", "glass")
        }
    }

    override fun init() {
        FxToolkit.registerStage { stage }
    }

    override fun start(stage: Stage) {
        app = SestimApplication()
        app.context = context
        app.`init JavaFX dependency injector`(context)
        app.start(stage)
    }

    override fun stop() {
        app.stop()
    }
}
