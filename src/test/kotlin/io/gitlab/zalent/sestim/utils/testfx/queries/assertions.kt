package io.gitlab.zalent.sestim.utils.testfx.queries

import io.gitlab.zalent.sestim.gui.ident.CssId
import io.gitlab.zalent.sestim.utils.testfx.CssClass
import javafx.scene.Node
import org.testfx.api.FxRobot
import org.testfx.service.query.NodeQuery
import kotlin.test.assertFalse
import kotlin.test.assertTrue

fun FxRobot.assertPresent(selector: String, cssId: CssId? = null, cssClass: CssClass? = null) {
    val nodeQuery = buildQuery(selector, cssClass, cssId)
    assertPresent(nodeQuery, selector)
}

fun FxRobot.assertPresent(selector: String, parentCssClass: CssClass) {
    val parentSelector = ".${parentCssClass.name}"
    assertPresent(selector, parentSelector = parentSelector)
}

fun FxRobot.assertPresent(selector: String, parentSelector: String) {
    val nodeParent = find(parentSelector)
    val nodeQuery = from(nodeParent).lookup(selector)
    assertPresent(nodeQuery, queryName = selector)
}

fun FxRobot.assertCheckboxSelected(cssId: CssId) {
    assertTrue(isCheckboxSelected(cssId), "Expected the checkbox (${cssId.name}) to be selected.")
}

fun FxRobot.assertCheckboxNotSelected(cssId: CssId) {
    assertFalse(isCheckboxSelected(cssId), "Expected the checkbox (${cssId.name}) to be not selected.")
}

@Suppress("unused")
fun FxRobot.assertPresent(nodeQuery: NodeQuery, queryName: String) {
    val isPresent = nodeQuery.tryQuery<Node>().isPresent
    assertTrue(isPresent, "expected the query `$queryName` to find a present node.")
}

fun FxRobot.assertNotPresent(selector: String, cssClass: CssClass? = null, cssId: CssId? = null) {
    assertNotPresent(buildQuery(selector, cssClass, cssId), queryName = selector)
}

@Suppress("unused")
fun FxRobot.assertNotPresent(nodeQuery: NodeQuery, queryName: String) {
    val isPresent = nodeQuery.tryQuery<Node>().isPresent
    assertFalse(isPresent, "expected the query `$queryName` to find no present nodes.")
}
