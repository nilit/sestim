package io.gitlab.zalent.sestim.utils.testfx.robot

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.default
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.queries.find
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import io.gitlab.zalent.sestim.utils.testfx.waitForNodeNotToBePresent
import org.testfx.api.FxRobot

object ProjectRobot : FxRobot() {
    fun create(
        projectName: String = default.project.name,
        pubId: String? = null,
        KLOC: String? = null
    ) {
        clickOn(id[ident.header.button.create])
        waitForNode(id[ident.project.edit.form])

        robot.writer.writeToField(id[ident.project.edit.field.name], projectName)
        if (pubId != null) {
            robot.writer.clearFieldAndWrite(id[ident.project.edit.field.pubId], pubId)
        }
        if (KLOC != null) {
            robot.writer.clearFieldAndWrite(id[ident.project.edit.field.KLOC], KLOC)
        }
        
        save()
        waitForNode(projectName, cssClass = css.cls.tableCell)
    }
    
    fun edit(
        name: String = default.project.name,
        nameNew: String? = null,
        KLOCNew: String? = null,
        stateNew: Project.State? = null
    ) {
        openEditForm(name)
        if (nameNew != null) {
            robot.writer.clearFieldAndWrite(id[ident.project.edit.field.name], nameNew)
        }
        if (KLOCNew != null) {
            robot.writer.clearFieldAndWrite(id[ident.project.edit.field.KLOC], KLOCNew)
        }
        if (stateNew != null) {
            robot.writer.select(ident.project.edit.field.state, stateNew.name)
        }
        save()
    }
    
    fun delete(name: String = default.project.name) {
        robot.contextmenu.openMenu(
            itemToSelect = name,
            menuOwnerSelector = id[ident.project.list.node],
            menuSelector = id[ident.project.list.contextmenu.node]
        )
        waitForNode(id[ident.project.list.contextmenu.delete])
        clickOn(id[ident.project.list.contextmenu.delete])
        waitForNode("OK")
        clickOn("OK")
        waitForNodeNotToBePresent(name, css.cls.tableCell)
    }
    
    fun save() {
        clickOn(id[ident.header.button.save])
        waitForNode(id[ident.project.list.node])
    }
    
    fun openEditForm(name: String = default.project.name) {
        clickOn(find(name, css.cls.tableCell))
        waitForNode(id[ident.project.edit.form])
    }
}
