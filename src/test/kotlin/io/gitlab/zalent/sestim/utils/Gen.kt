package io.gitlab.zalent.sestim.utils

import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.entity.Task
import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.database.repository.EstimateRepository
import io.gitlab.zalent.sestim.database.repository.ProjectRepository
import io.gitlab.zalent.sestim.database.repository.ProjectTypeRepository
import io.gitlab.zalent.sestim.database.repository.StoryRepository
import io.gitlab.zalent.sestim.database.repository.TaskRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

/**
 * A primitive entities generator.
 */
@Service
@Transactional
class Gen {
    @Autowired private lateinit var projectRepo: ProjectRepository
    @Autowired private lateinit var storyRepo: StoryRepository
    @Autowired private lateinit var estimateRepo: EstimateRepository
    @Autowired private lateinit var taskRepo: TaskRepository
    @Autowired private lateinit var projectTypeRepo: ProjectTypeRepository

    fun project(name: String? = null): Project {
        val project = Project(name = name ?: "test name")
        projectRepo.save(project)
        return project
    }

    fun story(
        name: String = default.story.name,
        state: PlanItemEntity.State? = null,
        project: Project? = null,
        estimates: List<Estimate> = listOf()
    ): Story {
        val story = Story(project = project ?: project(), name = name)
        if (state != null) story.state = state
        storyRepo.save(story)
        for (estimate in estimates) {
            estimate.story = story
            estimateRepo.save(estimate)
        }
        return story
    }

    fun task(
        name: String = default.task.name,
        state: PlanItemEntity.State? = null,
        estimates: List<Estimate> = listOf(),
        story: Story
    ): Task {
        val task = Task(name = name)
        if (state != null) task.state = state
        for (estimate in estimates) {
            estimate.task = task
            estimateRepo.save(estimate)
        }
        task.story = story
        taskRepo.save(task)
        return task
    }
    
    fun projectType(name: String = "type test"): Project.Type {
        val projectType = Project.Type(name = name)
        projectTypeRepo.save(projectType)
        return projectType
    }
    
    fun estimate(
        best: Int = 0, worst: Int = 0, expected: Int = 0, actual: Int = 0,
        project: Project? = null
    ): Estimate {
        val estimate = Estimate(
            best = Time(hours = best),
            worst = Time(hours = worst),
            expected = Time(hours = expected),
            actual = Time(hours = actual),
            project = project
        )
        estimateRepo.save(estimate)
        return estimate
    }
}
