package io.gitlab.zalent.sestim.utils

/**
 * They're all so short because the speed of typing it's quite a showstopper
 * with testfx.
 */
object default {
    object project {
        val name = "pt"
    }
    object story {
        val name = "st"
    }
    object task {
        val name = "tt"
    }
}
