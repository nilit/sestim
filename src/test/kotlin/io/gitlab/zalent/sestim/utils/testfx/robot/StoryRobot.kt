package io.gitlab.zalent.sestim.utils.testfx.robot

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.default
import io.gitlab.zalent.sestim.utils.testfx.css
import io.gitlab.zalent.sestim.utils.testfx.queries.find
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import io.gitlab.zalent.sestim.utils.testfx.waitForNodeNotToBePresent
import org.testfx.api.FxRobot

object StoryRobot : FxRobot() {
    fun create(
        name: String = default.story.name,
        description: String? = null,
        state: PlanItemEntity.State? = null
    ) {
        clickOn(id[ident.story.list.button.add])
        waitForNode(id[ident.story.edit.form])

        robot.writer.writeToField(id[ident.story.edit.field.name], name)
        if (description != null) {
            robot.writer.writeToField(id[ident.story.edit.field.desc.textarea], description)
        }
        if (state != null) {
            clickOn(id[ident.story.edit.field.state])
            waitForNode(state.toString())
            clickOn(state.toString())
        }
        
        save()
        waitForNode(name, cssClass = css.cls.tableCell)
    }
    
    fun edit(
        nameOld: String = default.story.name,
        nameNew: String? = null,
        typeNew: PlanItemEntity.Type? = null,
        sizeNew: PlanItemEntity.Size? = null,
        stateNew: PlanItemEntity.State? = null,
        LOCNew: String? = null
    ) {
        openEditForm(storyName = nameOld)
        if (nameNew != null) {
            robot.writer.clearFieldAndWrite(id[ident.story.edit.field.name], textToWrite = nameNew)
        }
        if (typeNew != null) robot.writer.select(ident.story.edit.field.type, typeNew.name)        
        if (sizeNew != null) robot.writer.select(ident.story.edit.field.size, sizeNew.name)
        if (stateNew != null) robot.writer.select(ident.story.edit.field.state, stateNew.name)
        if (LOCNew != null) {
            robot.writer.clearFieldAndWrite(id[ident.story.edit.field.LOC], textToWrite = LOCNew)
        }
        save()
    }
    
    fun delete(storyName: String = default.story.name) {
        robot.contextmenu.openMenu(
            itemToSelect = storyName,
            menuOwnerSelector = id[ident.story.list.node],
            menuSelector = id[ident.story.list.contextmenu.node]
        )
        clickOn(id[ident.story.list.contextmenu.delete])
        waitForNodeNotToBePresent(storyName, css.cls.tableCell)
    }
    
    fun openEditForm(storyName: String = default.story.name) {
        clickOnStoryInTable(storyName)
        waitForNode(id[ident.story.edit.form])
    }
    
    fun save() {
        clickOn(id[ident.header.button.save])
        waitForNode(id[ident.story.list.node])
    }
    
    private fun clickOnStoryInTable(storyName: String) {
        clickOn(find(storyName, cssClass = css.cls.tableCell))
    }
}
