package io.gitlab.zalent.sestim.utils.testfx.robot

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.utils.default
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import io.gitlab.zalent.sestim.utils.testfx.waitForNodeNotToBePresent
import org.testfx.api.FxRobot

object TaskRobot : FxRobot() {
    fun create(
        taskName: String = default.task.name,
        description: String? = null,
        state: PlanItemEntity.State? = null
    ) {
        clickOn(id[ident.task.list.button.create])
        waitForNode(id[ident.task.edit.form])
        robot.writer.writeToField(id[ident.task.edit.field.name], text = taskName)
        if (description != null) {
            robot.writer.writeToField(id[ident.task.edit.field.desc], text = description)
        }
        if (state != null) {
            clickOn(id[ident.task.edit.field.state])
            waitForNode(state.toString())
            clickOn(state.toString())
        }
        save()
    }
    
    fun edit(
        nameOld: String = default.task.name,
        nameNew: String? = null,
        sizeNew: PlanItemEntity.Size? = null,
        stateNew: PlanItemEntity.State? = null,
        LOCNew: String? = null
    ) {
        openEditForm(nameOld)
        if (nameNew != null) {
            robot.writer.clearFieldAndWrite(id[ident.task.edit.field.name], textToWrite = nameNew)
        }
        if (sizeNew != null) robot.writer.select(ident.task.edit.field.size, sizeNew.name)
        if (stateNew != null) robot.writer.select(ident.task.edit.field.state, stateNew.name)
        if (LOCNew != null) {
            robot.writer.clearFieldAndWrite(id[ident.task.edit.field.LOC], textToWrite = LOCNew)
        }
        save()
    }
    
    fun delete(taskName: String) {
        robot.contextmenu.openMenu(
            itemToSelect = taskName,
            menuOwnerSelector = id[ident.task.list.node],
            menuSelector = id[ident.task.list.contextmenu.delete]
        )
        clickOn(id[ident.task.list.contextmenu.delete])
        // TODO css class
        waitForNodeNotToBePresent(taskName)
    }
    
    fun openEditForm(taskName: String = default.task.name) {
        clickOn(taskName)
        waitForNode(id[ident.task.edit.form])
    }

    fun save() {
        clickOn(id[ident.header.button.save])
        waitForNode(id[ident.task.list.node])
    }
}
