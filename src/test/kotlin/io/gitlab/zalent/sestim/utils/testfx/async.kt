package io.gitlab.zalent.sestim.utils.testfx

import io.gitlab.zalent.sestim.gui.ident.CssId
import io.gitlab.zalent.sestim.utils.testfx.queries.buildQuery
import javafx.scene.Node
import org.testfx.api.FxRobot
import org.testfx.service.query.NodeQuery
import org.testfx.util.WaitForAsyncUtils
import java.util.concurrent.TimeUnit

fun FxRobot.waitForNode(selector: String, parentCssClass: CssClass) {
    waitForNode(selector, parentSelector = ".$parentCssClass")
}

fun FxRobot.waitForNode(selector: String, parentSelector: String) {
    val parentNodeQuery = lookup(parentSelector)
    val nodeQuery = from(parentNodeQuery).lookup(selector)
    waitForNode(nodeQuery)
}

fun FxRobot.waitForNode(
    selector: String, cssId: CssId? = null, cssClass: CssClass? = null, timeout: Int = 2000
) {
    val nodeQuery = buildQuery(selector, cssClass, cssId)
    waitForNode(nodeQuery, timeout)
}

fun waitForNode(nodeQuery: NodeQuery, timeout: Int = 2000) {
    waitFor(timeout) { nodeQuery.tryQuery<Node>().isPresent }
}

fun FxRobot.waitForNodeNotToBePresent(
    selector: String, cssClass: CssClass? = null, cssId: CssId? = null
) {
    val nodeQuery = buildQuery(selector, cssClass, cssId)
    waitFor { !nodeQuery.tryQuery<Node>().isPresent }
}

private fun waitFor(waitUntilMs: Int = 2000, callback: () -> Boolean) {
    WaitForAsyncUtils.waitFor(waitUntilMs.toLong(), TimeUnit.MILLISECONDS, callback)
}
