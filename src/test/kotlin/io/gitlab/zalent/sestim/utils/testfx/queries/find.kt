package io.gitlab.zalent.sestim.utils.testfx.queries

import io.gitlab.zalent.sestim.utils.testfx.CssClass
import javafx.scene.Node
import org.testfx.api.FxRobot

fun FxRobot.find(selector: String): Node? = lookup(selector).query()

fun FxRobot.find(selector: String, cssClass: CssClass): Node? {
    return buildQuery(selector, cssClass).query()
}
