package io.gitlab.zalent.sestim.utils.testfx

import io.gitlab.zalent.sestim.gui.ident.JavaFxSelector

object css {
    object cls {
        val tableCell = CssClass("table-cell")
        val contextMenu = CssClass("context-menu")
    }
    
    operator fun get(cssClass: CssClass): JavaFxSelector = ".${cssClass.name}"
}

data class CssClass(val name: String)
