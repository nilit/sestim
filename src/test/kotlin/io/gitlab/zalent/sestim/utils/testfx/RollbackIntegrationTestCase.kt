package io.gitlab.zalent.sestim.utils.testfx

import io.gitlab.zalent.sestim.database.repository.ProjectRepository
import io.gitlab.zalent.sestim.database.repository.StoryRepository
import io.gitlab.zalent.sestim.database.repository.TaskRepository
import org.junit.After
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired

/**
 * The `@Rollback` doesn't work (sometimes) for integration tests.
 */
abstract class RollbackIntegrationTestCase : IntegrationTestCase() {
    @Autowired private lateinit var taskRepo: TaskRepository
    @Autowired private lateinit var storyRepo: StoryRepository
    @Autowired private lateinit var projectRepo: ProjectRepository

    @Before
    @After
    fun deleteAllEntities() {
        taskRepo.deleteAll()
        storyRepo.deleteAll()
        projectRepo.deleteAll()
    }
}
