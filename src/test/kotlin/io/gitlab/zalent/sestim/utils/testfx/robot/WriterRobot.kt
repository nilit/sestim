package io.gitlab.zalent.sestim.utils.testfx.robot

import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.gui.ident.CssId
import io.gitlab.zalent.sestim.gui.ident.id
import io.gitlab.zalent.sestim.utils.testfx.queries.find
import io.gitlab.zalent.sestim.utils.testfx.queries.isCheckboxSelected
import io.gitlab.zalent.sestim.utils.testfx.waitForNode
import javafx.scene.control.CheckBox
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import org.testfx.api.FxRobot
import tornadofx.*

object WriterRobot : FxRobot() {
    /**
     * TestFX _sometimes_ after enabling a cell's edit mode (in any manner)
     * can go into a loop of pressing some keys (maybe the last one pressed)
     * over and over again.
     * 
     * This hack prevents that by avoiding [FxRobot.doubleClickOn] and
     * calling [releaseAllKeys] when necessary.
     */
    fun writeToCell(cellSelector: String, valueNew: String) {
        withKeysReset { 
            clickOn(cellSelector)
            clickOn(cellSelector)
        }
        withKeysReset { 
            press(KeyCode.BACK_SPACE)
            write(valueNew)
            press(KeyCode.ENTER)
        }
    }

    /** The same thing as in [writeToCell] only with [TextField]. */
    fun writeToTimeField(cssId: CssId, time: Time) {
        clickOn(id[cssId])
        
        clearTimeField()
        
        withKeysReset { write(time.hours.toString()) }
        withKeysReset { press(KeyCode.RIGHT) }
        withKeysReset { write(time.minutes.toString()) }
    }
    
    fun setCheckbox(cssId: CssId, isToSelect: Boolean) {
        val isSelected: Boolean = isCheckboxSelected(cssId)
        when {
            isSelected and !isToSelect -> clickOn(id[cssId])
            !isSelected and isToSelect -> clickOn(id[cssId])
        }
    }
    
    // TODO join with clearFieldAndWrite
    fun writeToField(inputQuery: String, text: String) {
        clickOn(inputQuery)
        write(text)
    }
    
    fun select(choiceboxId: CssId, itemText: String) {
        clickOn(id[choiceboxId])
        waitForNode(itemText)
        clickOn(itemText)
    }

    fun clearField(inputQuery: String) {
        doubleClickOn(inputQuery)
        press(KeyCode.BACK_SPACE)
    }

    fun clearFieldAndWrite(inputQuery: String, textToWrite: String) {
        doubleClickOn(inputQuery)
        write(textToWrite)
    }
    
    private fun clearTimeField() {
        withKeysReset { press(KeyCode.BACK_SPACE) }
        withKeysReset { press(KeyCode.BACK_SPACE) }
        withKeysReset { press(KeyCode.LEFT) }
        withKeysReset { press(KeyCode.BACK_SPACE) }
        withKeysReset { press(KeyCode.BACK_SPACE) }
        withKeysReset { press(KeyCode.BACK_SPACE) }
    }
    
    private fun withKeysReset(function: () -> Unit) {
        function()
        releaseAllKeys()
    }

    private fun releaseAllKeys() = release(*arrayOfNulls<KeyCode>(size = 0))
}
