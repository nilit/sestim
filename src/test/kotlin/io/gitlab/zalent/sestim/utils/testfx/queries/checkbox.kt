package io.gitlab.zalent.sestim.utils.testfx.queries

import io.gitlab.zalent.sestim.gui.ident.CssId
import io.gitlab.zalent.sestim.gui.ident.id
import org.testfx.api.FxRobot
import tornadofx.*

fun FxRobot.isCheckboxSelected(cssId: CssId) = find(id[cssId])!!.hasPseudoClass("selected")
