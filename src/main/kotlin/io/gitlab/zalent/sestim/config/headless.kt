package io.gitlab.zalent.sestim.config

val isHeadlessParamSet = System.getProperty("is_headless")?.toBoolean() ?: false

/** Because a bug in testfx/gradle enables it by default. */
fun configureHeadlessMode() {
    if (isHeadlessParamSet) {
        enableHeadlessMode()
    } else {
        disableHeadlessMode()
    }
}

private fun enableHeadlessMode() {
    System.setProperty("java.awt.headless", "true")
    System.setProperty("testfx.robot", "glass")
    System.setProperty("glass.platform", "Monocle")
    System.setProperty("monocle.platform", "Headless")
    System.setProperty("prism.order", "sw")
    System.setProperty("headless.geometry", "1600x1200-32")
}

private fun disableHeadlessMode() {
    System.setProperty("java.awt.headless", "false")
}
