package io.gitlab.zalent.sestim.domain.estimate.calc

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.service.EstimateCalcService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class EstimateCalcCreator {
    @Autowired private lateinit var calcService: EstimateCalcService
    @Autowired private lateinit var calculationService: EstimateCalcCollectionService
    
    fun create(estimate: Estimate, estimateOwnerEntity: EstimateOwnerEntity): Estimate.Calc {
        val estimateCalc = Estimate.Calc(estimate = estimate)
        estimateCalc += calculationService.collect(estimateOwnerEntity)
        calcService.save(estimateCalc)
        return estimateCalc
    }
}
