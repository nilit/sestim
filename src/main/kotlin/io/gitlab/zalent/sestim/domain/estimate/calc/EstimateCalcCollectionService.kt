package io.gitlab.zalent.sestim.domain.estimate.calc

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.entity.Task
import io.gitlab.zalent.sestim.database.service.EstimateService
import io.gitlab.zalent.sestim.database.service.StoryService
import io.gitlab.zalent.sestim.database.service.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class EstimateCalcCollectionService {
    @Autowired private lateinit var storyService: StoryService
    @Autowired private lateinit var taskService: TaskService
    @Autowired private lateinit var estimateService: EstimateService
    
    fun collect(estimateOwner: EstimateOwnerEntity) = when (estimateOwner) {
        is Project -> collect(project = estimateOwner)
        is Story -> collect(story = estimateOwner)
        else -> throw IllegalArgumentException()
    }

    fun collect(project: Project): Estimate.Calc {
        val estimateSum = Estimate.Calc()
        val stories = storyService.getByProject(project)
        for (story: Story in stories) {
            if (story.isNotActive) continue
            val storyEstimates = estimateService.getByEstimateOwner(story)
            estimateSum += findAnyLastValidEstimate(storyEstimates)
            
            val tasksCalculation = collect(story)
            estimateSum += tasksCalculation
        }
        return estimateSum
    }
    
    fun collect(story: Story): Estimate.Calc {
        val estimateSum = Estimate.Calc()
        val tasks = taskService.getByStory(story)
        for (task: Task in tasks) {
            if (task.isNotActive) continue
            val taskEstimates = estimateService.getByEstimateOwner(task)
            estimateSum += findAnyLastValidEstimate(taskEstimates)
        }
        return estimateSum
    }
    
    private fun findAnyLastValidEstimate(estimates: List<Estimate>): Estimate? {
        var estimateFound: Estimate? = null
        val estimatesSortedNewToOld = estimates.sortedByDescending { it.dateCreated }
        for (estimate in estimatesSortedNewToOld) {
            val isShouldBeCounted: Boolean = estimate.isSufficientlyFilled
            if (isShouldBeCounted) {
                estimateFound = estimate
                return estimateFound
            }
        }
        return estimateFound
    }
}
