package io.gitlab.zalent.sestim.gui.view.estimate

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.gui.control.timefield
import io.gitlab.zalent.sestim.gui.controller.EstimateController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.EstimateModel
import io.gitlab.zalent.sestim.gui.service.list.EstimateListService
import io.gitlab.zalent.sestim.gui.service.nav.viewnode.ViewNodeTreeService
import io.gitlab.zalent.sestim.gui.util.runAsyncWithIndicator
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import tornadofx.*

/**
 * It was split on subclasses to preserve the current breadcrumbs render.
 * 
 * Because if there were only one [EstimateEditView], then no matter where
 * the user would be (project edit view, story edit view, etc) his breadcrumbs
 * in the [EstimateEditView] would always show `{project_name} > estimate edit`.
 * That would happen because [ViewNodeTreeService] returns the fist and the highest
 * node matched, and don't expect to find several views of the same class. And
 * because the breadcrumbs render logic is bound to [ViewNodeTreeService] the
 * breadcrumbs view won't be able to differentiate between the same [EstimateEditView].
 */
abstract class EstimateEditView : ViewDockerized() {
    private val estimateModel = EstimateModel(Estimate())

    val estimate: Estimate by param()
    val estimateOwner: EstimateOwnerEntity by param()
    
    private val calcListFragment: EstimateCalcListFragment = EstimateCalcListFragment()

    private val estimateListService: EstimateListService by di()
    private val controller: EstimateController by di()
    
    override val root = form {
        id = ident.estimate.edit.form.name
        
        fieldset {
            hbox(40) {
                vbox {
                    field("best") {
                        timefield(estimateModel.best) {
                            id = ident.estimate.edit.field.best.name
                        }
                    }
                    field("worst") {
                        timefield(estimateModel.worst) {
                            id = ident.estimate.edit.field.worst.name
                        }
                    }
                    field("expected") {
                        timefield(estimateModel.expected) {
                            id = ident.estimate.edit.field.expected.name
                        }
                    }
                    field("actual") {
                        timefield(estimateModel.actual) {
                            id = ident.estimate.edit.field.actual.name
                        }
                    }
                }
                vbox {
                    field {
                        checkbox("is date created valid", estimateModel.isDateValid) {
                            id = ident.estimate.edit.field.isDateValid.name
                        }
                    }
                    field {
                        checkbox("manually adjusted", estimateModel.isAdjusted)
                    }
                    field("created") {
                        label(estimateModel.dateCreated)
                    }
                    field("modified") {
                        label(estimateModel.dateModified)
                    }
                }
            }
        }
        fieldset {
            add(calcListFragment)
        }
    }
    
    init {
        controlButtons.isSaveEnabled.bind(estimateModel.valid)
    }
    
    override fun onDock() {
        estimateModel.rebind { item = estimate }
        
        initChildrenCalcListView()
    }
    
    override fun onSave() {
        runAsyncWithIndicator {
            controller.save(estimateModel)
            estimateListService.updateList(estimateOwner)
        }
        viewDocker.navigateBack()
    }
    
    private fun initChildrenCalcListView() {
        calcListFragment.parentEstimateProperty.value = estimate
        calcListFragment.estimateOwner = estimateOwner
    }
}
