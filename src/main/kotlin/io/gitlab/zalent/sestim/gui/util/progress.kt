package io.gitlab.zalent.sestim.gui.util

import io.gitlab.zalent.sestim.gui.view.header.ProgressIndicatorView
import tornadofx.*

fun <T> Component.runAsyncWithIndicator(func: FXTask<*>.() -> T) {
    val progressIndicatorView = find<ProgressIndicatorView>()
    progressIndicatorView.start()
    runAsync(func = func).ui {
        progressIndicatorView.stop()
    }
}
