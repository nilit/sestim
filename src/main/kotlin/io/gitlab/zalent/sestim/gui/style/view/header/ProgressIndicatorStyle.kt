package io.gitlab.zalent.sestim.gui.style.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import tornadofx.*

class ProgressIndicatorStyle : Stylesheet() {
    private val indicator by cssid(ident.header.addressbar.progressIndicator.name)
    private val indicatorVisible by csspseudoclass(ProgressIndicatorStyle.pseudoClasses.visible)
    
    object pseudoClasses {
        val visible = "indicator-visible"
    }
    
    init {
        indicator {
            val size = 16.px
            maxHeight = size
            maxWidth = size
            visibility = FXVisibility.HIDDEN
            
            and(indicatorVisible) {
                visibility = FXVisibility.VISIBLE
            }
        }
    }
}
