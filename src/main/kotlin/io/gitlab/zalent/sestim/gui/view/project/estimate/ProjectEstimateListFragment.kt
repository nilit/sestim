package io.gitlab.zalent.sestim.gui.view.project.estimate

import io.gitlab.zalent.sestim.gui.view.estimate.EstimateListFragment

class ProjectEstimateListFragment : EstimateListFragment() {
    override val estimateEditViewClass = ProjectEstimateEditView::class
}
