package io.gitlab.zalent.sestim.gui.controller

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.model.ProjectModel
import io.gitlab.zalent.sestim.gui.model.StoryModel
import io.gitlab.zalent.sestim.database.service.StoryService
import io.gitlab.zalent.sestim.gui.service.list.ProjectListService
import io.gitlab.zalent.sestim.gui.service.list.StoryListService
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import tornadofx.*
import javax.annotation.PostConstruct

@Controller
class StoryEditController {
    lateinit var projectListProperty: SimpleListProperty<Project>
    
    @Autowired private lateinit var storyService: StoryService
    @Autowired private lateinit var storyListService: StoryListService
    @Autowired private lateinit var projectListService: ProjectListService
    
    @PostConstruct
    fun init() {
        initProjectListProperty()
    }
    
    fun save(storyModel: StoryModel) {
        val isStoryMovedToAnotherProject = storyModel.project.isDirty
        if (isStoryMovedToAnotherProject) {
            val projectOld: Project = storyModel.item.project
            saveStoryModel(storyModel)
            storyListService.updateList(projectOld)
        } else {
            saveStoryModel(storyModel)
            val projectNew = storyModel.item.project
            storyListService.updateList(projectNew)
        }
    }
    
    private fun saveStoryModel(storyModel: StoryModel) {
        storyModel.commit()
        storyService.save(storyModel.item)
    }
    
    private fun initProjectListProperty() {
        projectListProperty = `create initial list for projectListProperty`()
        `bind projectListProperty to ProjectListService onChange`(projectListProperty)
    }
    
    private fun `create initial list for projectListProperty`(): SimpleListProperty<Project> {
        val projectList = projectListService.listProperty.map { it.item }
        val projectListObservable = FXCollections.observableList(projectList)
        return SimpleListProperty<Project>(projectListObservable)
    }
    
    private fun `bind projectListProperty to ProjectListService onChange`(
        projectListProperty: SimpleListProperty<Project>
    ) {
        projectListService.listProperty.onChange<ProjectModel> { listChange ->
            val projectList: List<Project> = listChange.list.map { it.item }
            projectListProperty.setAll(projectList)
        }
    }
}
