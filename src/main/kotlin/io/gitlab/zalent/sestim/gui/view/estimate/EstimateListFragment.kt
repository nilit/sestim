package io.gitlab.zalent.sestim.gui.view.estimate

import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.gui.control.columnWithId
import io.gitlab.zalent.sestim.gui.control.extension.onMouseLeftButtonClickAtItem
import io.gitlab.zalent.sestim.gui.control.tableviewWithoutEmptyRows
import io.gitlab.zalent.sestim.gui.controller.EstimateController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.EstimateModel
import io.gitlab.zalent.sestim.gui.property.ObjectPropertyNoisy
import io.gitlab.zalent.sestim.gui.service.list.EstimateListService
import io.gitlab.zalent.sestim.gui.view.ViewDocker
import javafx.scene.control.TableView
import javafx.scene.layout.Priority
import tornadofx.*
import kotlin.reflect.KClass

abstract class EstimateListFragment : Fragment() {
    abstract val estimateEditViewClass: KClass<out EstimateEditView>
    
    val estimateOwnerProperty = ObjectPropertyNoisy<EstimateOwnerEntity>()
    private var estimateOwner: EstimateOwnerEntity by estimateOwnerProperty

    private val viewDocker: ViewDocker by inject()

    private val estimateListService: EstimateListService by di()
    private val controller: EstimateController by di()
    
    override val root = vbox {
        hbox {
            id = ident.estimate.list.header.container.name

            label("Estimates") {
                id = ident.estimate.list.header.label.name
            }
            hbox { hgrow = Priority.ALWAYS }
            button("add an estimate") {
                id = ident.estimate.list.button.add.name
                action { create() } 
            }
        }
        
        // TODO try to remove the min width were there is no need for it
        tableviewWithoutEmptyRows(estimateListService.listProperty) {
            id = ident.estimate.list.node.name
            
            columnWithId(EstimateModel::bestString, name = EstimateModel::best.name) {
                minWidth = 65.0
            }
            columnWithId(EstimateModel::worstString, name = EstimateModel::worst.name) {
                minWidth = 65.0
            }
            columnWithId(EstimateModel::expectedString, name = EstimateModel::expected.name) {
                minWidth = 70.0
            }
            columnWithId(EstimateModel::actualString, name = EstimateModel::actual.name) {
                minWidth = 65.0
            }
            column<EstimateModel, Boolean>("adjusted", EstimateModel::isAdjusted) {
                minWidth = 70.0
            }
            column<EstimateModel, String>("modified", EstimateModel::dateModified) {
                minWidth = 110.0
            }

            contextmenu {
                id = ident.estimate.list.contextmenu.node.name
                item("edit") {
                    id = ident.estimate.list.contextmenu.edit.name
                    action { openEditView(selectedItem!!) }
                }
                item("delete") {
                    id = ident.estimate.list.contextmenu.delete.name
                    action {
                        // TODO progress indicator
                        controller.delete(estimateModel = selectionModel.selectedItem)
                    }
                }
            }
            
            onMouseLeftButtonClickAtItem {
                openEditView(selectedItem!!)
            }

            placeholder = label("no estimates assigned")
            
            columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
        }
    }

    override fun onRefresh() {
        estimateListService.updateList(estimateOwner)
    }
    
    init {
        estimateOwnerProperty.onNonNullAssignment { ownerNew ->
            estimateListService.updateList(ownerNew)
        }
    }
    
    private fun create() {
        val estimateModel = controller.create(estimateOwner)
        openEditView(estimateModel)
    }
    
    private fun openEditView(estimateModel: EstimateModel) {
        val params = mapOf(
            EstimateEditView::estimateOwner.name to estimateOwner,
            EstimateEditView::estimate.name to estimateModel.item
        )
        viewDocker.dock(estimateEditViewClass, params)
    }
}
