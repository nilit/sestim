package io.gitlab.zalent.sestim.gui.service.nav

import io.gitlab.zalent.sestim.gui.property.ObjectPropertyNoisy
import io.gitlab.zalent.sestim.gui.service.nav.viewnode.ViewNode
import io.gitlab.zalent.sestim.gui.service.nav.viewnode.ViewNodeTreeService
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import tornadofx.*
import java.util.*
import javax.annotation.PostConstruct
import kotlin.reflect.KClass

@Service
class HistoryService {
    /** the main storage of history */
    lateinit var historyStackProp: StackObservable<ViewNode>
    
    lateinit var viewNodeCurrentProp: ObjectPropertyNoisy<ViewNode>
    lateinit var viewCurrentProp: ObjectPropertyNoisy<ViewDockerized>
    lateinit var isAtRootViewProp: SimpleBooleanProperty

    @Autowired private lateinit var nodeTreeService: ViewNodeTreeService

    @PostConstruct
    fun init() {
        historyStackProp = `create historyStackProp`()
        viewNodeCurrentProp = `create and init viewNodeCurrentProp`(historyStackProp)
        viewCurrentProp = `create and init viewCurrentProp`(viewNodeCurrentProp)
        isAtRootViewProp = `create and init isAtRootViewProp`(viewNodeCurrentProp)
    }
    
    fun pushView(viewNew: KClass<out View>, params: Map<String, Any>? = null) {
        val viewNodeNew = nodeTreeService.findNode(viewNew)!!
        if (params != null) viewNodeNew.params.putAll(params)
        historyStackProp.push(viewNodeNew)
    }

    fun popView(): ViewDockerized {
        historyStackProp.pop()
        val viewNodePrevious = historyStackProp.peek()
        val viewPrevious = find(
            type = viewNodePrevious.viewClass,
            params = viewNodePrevious.params
        )
        return viewPrevious
    }

    private fun `create historyStackProp`(): StackObservable<ViewNode> {
        val historyStackProp = Stack<ViewNode>()
        historyStackProp.push(nodeTreeService.rootnode)
        return StackObservable(historyStackProp)
    }
    
    private fun `create and init viewNodeCurrentProp`(
        historyStackProp: StackObservable<ViewNode>
    ): ObjectPropertyNoisy<ViewNode> {
        viewNodeCurrentProp = ObjectPropertyNoisy(nodeTreeService.rootnode)
        this.historyStackProp.onChange {
            val viewNodeCurrent = this.historyStackProp.peek()
            viewNodeCurrentProp.value = viewNodeCurrent
        }
        return viewNodeCurrentProp
    }

    private fun `create and init viewCurrentProp`(
        viewNodeCurrentProp: ObjectPropertyNoisy<ViewNode>
    ): ObjectPropertyNoisy<ViewDockerized> {
        viewCurrentProp = ObjectPropertyNoisy()
        this.viewNodeCurrentProp.onNonNullAssignment { viewNodeCurrentNew ->
            val viewCurrentNew = find(
                viewNodeCurrentNew.viewClass,
                params = viewNodeCurrentNew.params
            )
            viewCurrentProp.value = viewCurrentNew
        }
        return viewCurrentProp
    }

    private fun `create and init isAtRootViewProp`(
        viewNodeCurrentProp: SimpleObjectProperty<ViewNode>
    ): SimpleBooleanProperty {
        isAtRootViewProp = SimpleBooleanProperty(true)
        this.viewNodeCurrentProp.onChange { nodeCurrent ->
            val isAtRoot = nodeCurrent == nodeTreeService.rootnode
            isAtRootViewProp.value = isAtRoot
        }
        return isAtRootViewProp
    }
}
