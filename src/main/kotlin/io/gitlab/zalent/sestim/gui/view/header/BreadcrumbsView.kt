package io.gitlab.zalent.sestim.gui.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.service.nav.HistoryService
import io.gitlab.zalent.sestim.gui.service.nav.viewnode.ViewNode
import io.gitlab.zalent.sestim.gui.service.nav.viewnode.ViewNodeTreeService
import io.gitlab.zalent.sestim.gui.style.view.header.BreadcrumbsStyle
import javafx.beans.property.Property
import javafx.beans.property.SimpleStringProperty
import javafx.collections.ObservableList
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.Priority
import tornadofx.*
import kotlin.reflect.KClass

class BreadcrumbsView : View() {
    private val historyService: HistoryService by di()
    private val nodeTreeService: ViewNodeTreeService by di()
    
    override val root = hbox {
        id = ident.header.addressbar.breadcrumbs.container.name
        hgrow = Priority.ALWAYS
    }
    
    init {
        buildBreadcrumbs(nodeTreeService.rootnode, isCurrentView = true)
        
        `rerender on currentNodeView change`()
    }
    
    fun `bind view's breadcrumb name to stringProperty`(
        viewClass: KClass<out View>,
        stringProp: Property<String>
    ) {
        val breadcrumbViewNode = nodeTreeService.findNode(viewClass)!!
        breadcrumbViewNode.nameProperty.bind(stringProp)
    }

    private fun rerender(node: ViewNode) {
        root.children.clear()
        buildBreadcrumbs(node, isCurrentView = true)
    }

    private fun buildBreadcrumbs(node: ViewNode, isCurrentView: Boolean = false) {
        addBreadcrumbItem(node.nameProperty, isCurrentView)

        val isNotTheLastItem = node != historyService.viewNodeCurrentProp.value
        if (isNotTheLastItem) addArrowIcon()
        
        if (node.parent != null) {
            buildBreadcrumbs(node.parent!!)
        }
    }
    
    private fun addBreadcrumbItem(nameProp: SimpleStringProperty, isCurrentView: Boolean = false) {
        val breadcrumbsItem = Label()
        breadcrumbsItem.textProperty().bind(nameProp)
        breadcrumbsItem.styleClass += BreadcrumbsStyle.classes.item
        if (isCurrentView) {
            breadcrumbsItem.styleClass += BreadcrumbsStyle.classes.itemCurrent
        }
        root.children.insertAtStart(breadcrumbsItem)
    }
    
    private fun addArrowIcon() {
        val arrowIcon = Label()
        arrowIcon.styleClass += BreadcrumbsStyle.classes.arrowIcon
        root.children.add(1, arrowIcon)
    }
    
    private fun `rerender on currentNodeView change`() {
        historyService.viewNodeCurrentProp.onChange { node: ViewNode? ->
            rerender(node!!)
        }
    }
    
    private fun ObservableList<Node>.insertAtStart(node: Node) = this.add(0, node)
}
