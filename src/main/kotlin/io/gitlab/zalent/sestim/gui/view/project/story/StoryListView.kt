package io.gitlab.zalent.sestim.gui.view.project.story

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.control.extension.onMouseLeftButtonClickAtItem
import io.gitlab.zalent.sestim.gui.control.tableviewWithoutEmptyRows
import io.gitlab.zalent.sestim.gui.controller.StoryListController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.StoryModel
import io.gitlab.zalent.sestim.gui.service.list.StoryListService
import io.gitlab.zalent.sestim.gui.property.ObjectPropertyNoisy
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.scene.control.TableView
import javafx.scene.layout.Priority
import tornadofx.*

class StoryListView : ViewDockerized() {
    val projectProperty = ObjectPropertyNoisy<Project>()
    private var project: Project by projectProperty

    private val controller: StoryListController by di()
    private val storyListService: StoryListService by di()

    override val root = fieldset {
        id = ident.story.list.container.name
        
        vbox {
            hbox {
                id = ident.story.list.header.container.name

                label("Stories") {
                    id = ident.story.list.header.label.name
                }
                hbox { hgrow = Priority.ALWAYS }
                button("add a story") {
                    id = ident.story.list.button.add.name
                    action { openStoryCreateView(project = project) }
                }
            }
            tableviewWithoutEmptyRows(storyListService.listProperty) {
                id = ident.story.list.node.name

                column<StoryModel, String>("name", StoryModel::name) {
                    minWidth = 225.0
                }
                column("type", StoryModel::type)
                column("state", StoryModel::state)
                
                contextmenu {
                    id = ident.story.list.contextmenu.node.name
                    item("edit") {
                        id = ident.story.list.contextmenu.edit.name
                        action { openStoryEditView(selectedItem!!) }
                    }
                    item("delete") {
                        id = ident.story.list.contextmenu.delete.name
                        action { controller.delete(selectedItem!!) }
                    }
                }
                
                onMouseLeftButtonClickAtItem {
                    openStoryEditView(selectedItem!!)
                }
                
                placeholder = label("no stories yet")

                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
            }
        }
    }

    init {
        projectProperty.onNonNullAssignment { project ->
            storyListService.updateList(project)
        }
    }

    private fun openStoryCreateView(project: Project) {
        val params = mapOf(StoryCreateView::project.name to project)
        viewDocker.dock(StoryCreateView::class, params)
    }

    private fun openStoryEditView(storyModel: StoryModel) {
        val params = mapOf(StoryEditView::story.name to storyModel.item)
        viewDocker.dock(StoryEditView::class, params)
    }
}
