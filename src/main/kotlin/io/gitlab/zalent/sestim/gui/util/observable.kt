package io.gitlab.zalent.sestim.gui.util

import javafx.beans.InvalidationListener
import javafx.beans.property.ObjectProperty

fun <T> ObjectProperty<T>.onInvalidation(callback: () -> Unit) = apply {
    val invalidationListener = InvalidationListener {
        callback()
    }
    addListener(invalidationListener)
}
