package io.gitlab.zalent.sestim.gui.style

import io.gitlab.zalent.sestim.gui.style.view.RootStyle
import io.gitlab.zalent.sestim.gui.style.view.TableViewStyle
import io.gitlab.zalent.sestim.gui.style.view.header.DockControlPanelStyle
import io.gitlab.zalent.sestim.gui.style.view.project.ProjectListStyle
import io.gitlab.zalent.sestim.gui.style.view.project.story.StoryEditStyle
import io.gitlab.zalent.sestim.gui.style.view.project.story.StoryListStyle
import io.gitlab.zalent.sestim.gui.style.view.project.estimate.EstimateListStyle
import io.gitlab.zalent.sestim.gui.style.view.project.story.task.TaskListStyle
import io.gitlab.zalent.sestim.config.isHeadlessParamSet
import io.gitlab.zalent.sestim.gui.style.view.header.AddressBarStyle
import io.gitlab.zalent.sestim.gui.style.view.header.BreadcrumbsStyle
import io.gitlab.zalent.sestim.gui.style.view.header.ProgressIndicatorStyle
import tornadofx.*
import java.awt.GraphicsEnvironment

fun configureAppSize() {
    FX.primaryStage.minWidth = 650.0
    FX.primaryStage.width = 750.0
    
    val heightMin = 700.0
    FX.primaryStage.minHeight = heightMin
    FX.primaryStage.height = if (isHeadlessParamSet) {
        heightMin
    } else {
        getHeightMaxAdjusted()
    }
}

fun importAppStylesheets() {
    importStylesheet("/css/reset.css")
    importStylesheet<RootStyle>()
    importStylesheet<TableViewStyle>()
    
    importStylesheet<DockControlPanelStyle>()
        importStylesheet<AddressBarStyle>()
            importStylesheet<ProgressIndicatorStyle>()
        importStylesheet<BreadcrumbsStyle>()
    
    importStylesheet<ProjectListStyle>()
        importStylesheet<StoryListStyle>()
            importStylesheet<StoryEditStyle>()
                importStylesheet<TaskListStyle>()
                importStylesheet<EstimateListStyle>()
}

private fun getHeightMaxAdjusted(): Double {
    val graphicEnvLocal = GraphicsEnvironment.getLocalGraphicsEnvironment()
    val windowSizeMaxWithoutToolbars = graphicEnvLocal.maximumWindowBounds
    val heightMaxWithoutToolbars = windowSizeMaxWithoutToolbars.height
    val heightMaxAdjusted = heightMaxWithoutToolbars * 0.80
    return heightMaxAdjusted
}
