package io.gitlab.zalent.sestim.gui.style.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import javafx.geometry.Pos
import tornadofx.*

class BreadcrumbsStyle : Stylesheet() {
    private val container by cssid(ident.header.addressbar.breadcrumbs.container.name)
    
    private val breadcrumb by cssclass(classes.item)
    private val breadcrumbCurrent by cssclass(classes.itemCurrent)
    private val arrowIcon by cssclass(classes.arrowIcon)
    
    object classes {
        val item = "breadcrumbs-item"
        val itemCurrent = "breadcrumbs-item-current"
        val arrowIcon = "breadcrumbs-arrow-icon"
    }
    
    init {
        container {
            alignment = Pos.CENTER_LEFT
            
            breadcrumb {
                text {
                    fill = c("898989")
                }
                padding = box(top = 0.px, right = 7.px, left = 7.px, bottom = 0.px)
                
                and(breadcrumbCurrent) {
                    text {
                        fill = c("4d4d4d")
                    }
                }
            }
            // TODO move it lower, as in gitlab
            arrowIcon {
                val width = 4.px
                val height = 6.px
                minWidth = width
                minHeight = height
                maxWidth = width
                maxHeight = height
                shape = "M10.243 8l-4.95-4.95a1 1 0 0 1 1.414-1.414l5.657 5.657a.997.997 0 0 1 0 1.414l-5.657 5.657a1 1 0 0 1-1.414-1.414L10.243 8z"
                backgroundColor += c("000")
            }
        }
    }
}
