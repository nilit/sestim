package io.gitlab.zalent.sestim.gui.style.view

import io.gitlab.zalent.sestim.gui.style.NotoSans
import io.gitlab.zalent.sestim.gui.style.stylesheet.TableStylesheet
import io.gitlab.zalent.sestim.gui.style.stylesheet.box
import javafx.geometry.Pos
import javafx.scene.Cursor
import javafx.scene.paint.Color
import tornadofx.*

class TableViewStyle : TableStylesheet() {
    init {
        `reset defaults`()

        tableView {
            tableColumn {
                padding = box(6)
            }
            
            tableHeaderRow {
                tableHeaderColumns {
                    tableHeaderColumn {
                        label {
                            alignment = Pos.CENTER_LEFT
                            padding = box(bottom = 6)
                            font = NotoSans.regular
                            fontSize = 13.px
                            textFill = c("#929292")
                        }
                        borderWidth += box(bottom = 1)
                        borderColor += box(c("DDDDDD"))
                        backgroundColor += Color.TRANSPARENT
                    }
                }
            }
            
            tableRowContainer {
                and(hover) {
                    cursor = Cursor.HAND
                }
                tableRow {
                    borderColor += box(
                        top = Color.TRANSPARENT,
                        right = Color.TRANSPARENT,
                        left = Color.TRANSPARENT,
                        bottom = c("eaecef")
                    )
                    borderWidth += box(bottom = 1)
                    and(hover) {
                        backgroundColor += c("f6f8fa")
                    }
                }
            }
        }
    }

    private fun `reset defaults`() {
        tableView {
            backgroundColor += Color.TRANSPARENT
            
            and(focused) {
                backgroundRadius += box(0.px)
                backgroundColor += Color.TRANSPARENT
            }
            
            tableHeaderRow {
                backgroundColor += Color.TRANSPARENT
                tableHeaderRowFiller {
                    backgroundColor += Color.TRANSPARENT
                }
            }
            tableRow {
                backgroundColor += Color.TRANSPARENT
                tableCell {
                    borderColor += box(Color.TRANSPARENT)
                    borderWidth += box(0.px)
                }
            }
        }
    }
}
