package io.gitlab.zalent.sestim.gui.view.estimate

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.gui.control.columnWithId
import io.gitlab.zalent.sestim.gui.control.tableviewWithoutEmptyRows
import io.gitlab.zalent.sestim.gui.controller.EstimateCalcController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.EstimateCalcModel
import io.gitlab.zalent.sestim.gui.property.ObjectPropertyNoisy
import io.gitlab.zalent.sestim.gui.service.list.EstimateCalcListService
import javafx.scene.control.TableView
import javafx.scene.layout.Priority
import tornadofx.*

class EstimateCalcListFragment : Fragment() {
    lateinit var estimateOwner: EstimateOwnerEntity
    
    val parentEstimateProperty = ObjectPropertyNoisy<Estimate>()
    var parentEstimate: Estimate by parentEstimateProperty

    private val calcListService: EstimateCalcListService by di()
    private val controller: EstimateCalcController by di()
    
    override val root = vbox {
        hbox {
            id = ident.estimate.calcList.header.container.name

            label("Calculations based on sub estimates") {
                id = ident.estimate.calcList.header.label.name
            }
            hbox { hgrow = Priority.ALWAYS }
            button("calculate") {
                id = ident.estimate.calcList.button.calculate.name
                action { controller.create(parentEstimate, estimateOwner) }
            }
        }
        
        tableviewWithoutEmptyRows(calcListService.listProperty) {
            id = ident.estimate.calcList.node.name
            
            columnWithId(EstimateCalcModel::best) {
                minWidth = 60.0
            }
            columnWithId(EstimateCalcModel::worst) {
                minWidth = 80.0
            }
            columnWithId(EstimateCalcModel::expected) {
                minWidth = 90.0
            }
            columnWithId(EstimateCalcModel::actual) {
                minWidth = 80.0
            }
            column<EstimateCalcModel, String>("created", EstimateCalcModel::dateCreated) {
                minWidth = 120.0
            }

            contextmenu {
                id = ident.estimate.calcList.contextmenu.node.name
                item("delete") {
                    id = ident.estimate.calcList.contextmenu.delete.name
                    action {
                        // TODO progress indicator
                        controller.delete(calcModel = selectionModel.selectedItem)
                    }
                }
            }

            placeholder = label("no calculations yet")
            
            columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
        }
    }

    override fun onRefresh() {
        calcListService.updateList(parentEstimate)
    }
    
    init {
        parentEstimateProperty.onNonNullAssignment { parentEstimateNew ->
            calcListService.updateList(parentEstimateNew)
        }
    }
}
