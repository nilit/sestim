package io.gitlab.zalent.sestim.gui.property

import javafx.beans.property.SimpleObjectProperty

/**
 * [SimpleObjectProperty] won't call [fireValueChangedEvent]
 * if `valueOld == valueNew`, which is misleading as hell.
 * Even the JavaFx team have screwed it up at least once and had to release
 * a hotfix.
 */
class ObjectPropertyNoisy<T>(initialValue: T? = null) : SimpleObjectProperty<T>(initialValue) {
    private val listeners = mutableListOf<(T) -> Unit>()
    
    fun onNonNullAssignment(listener: (T) -> Unit) {
        listeners.add(listener)
    }
    
    override fun set(valueNew: T) {
        super.set(valueNew)
        if (valueNew != null) fireOnNonNullAssignmentEvent(valueNew)
    }
    
    private fun fireOnNonNullAssignmentEvent(valueNew: T) {
        listeners.forEach { listener -> listener(valueNew) }
    }
}
