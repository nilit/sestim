package io.gitlab.zalent.sestim.gui.control

import com.sun.javafx.scene.control.skin.TableViewSkin
import com.sun.javafx.scene.control.skin.VirtualFlow
import io.gitlab.zalent.sestim.gui.util.onInvalidation
import javafx.beans.property.ObjectProperty
import javafx.beans.property.ReadOnlyListProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.collections.ObservableList
import javafx.event.EventTarget
import javafx.scene.control.IndexedCell
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import tornadofx.*

fun <RowItem> EventTarget.tableviewWithoutEmptyRows(
    items: ReadOnlyListProperty<RowItem>,
    body: (TableViewWithoutEmptyRows<RowItem>.() -> Unit)? = null
): TableViewWithoutEmptyRows<RowItem> {
    val tableview = TableViewWithoutEmptyRows<RowItem>()
    tableview.items = items
    return opcr(parent = this, node = tableview, op = body)
}

// TODO create a gist
class TableViewWithoutEmptyRows<RowItem> : TableView<RowItem>() {
    /**
     * Allows [TableViewSkinWithoutEmptyRows] to compute the preferable height
     * based on the rows count.
     */
    val rowsCountProp = SimpleIntegerProperty()

    private val itemsBindingProp: ObjectProperty<ObservableList<RowItem>>
        get() = itemsProperty()
    
    init {
        `adjust rowsCount on table's items change`()
    }

    override fun createDefaultSkin() = TableViewSkinWithoutEmptyRows(tableView = this)

    private fun `adjust rowsCount on table's items change`() {
        // An InvalidationListener, because [ObjectProperty] doesn't fire its
        // listeners if `oldValue == newValue`
        itemsBindingProp.onInvalidation {
            val listBound = itemsBindingProp.value
            rowsCountProp.value = listBound.size
            
            listBound.onChange { listChange ->
                rowsCountProp.value = listChange.list.size
            }
        }
    }
}

class TableViewSkinWithoutEmptyRows<CellItem>(
    tableView: TableViewWithoutEmptyRows<CellItem>
) : TableViewSkin<CellItem>(tableView) {
    private val tableView: TableViewWithoutEmptyRows<CellItem>
        get() = skinnable as TableViewWithoutEmptyRows<CellItem>

    override fun computePrefHeight(
        width: Double,
        topInset: Double, rightInset: Double, bottomInset: Double, leftInset: Double
    ): Double {
        val rowsCount: Int = tableView.rowsCountProp.value
        val tableBodyHeight = `get table body height based on rows count`(rowsCount)
        val tableHeaderHeight = tableHeaderRow.prefHeight(width)
        val tableHeight = tableBodyHeight + tableHeaderHeight
        return tableHeight
    }
    
    override fun createVirtualFlow() = FlowWithHeightCalc<TableRow<CellItem>>()

    private fun `get table body height based on rows count`(rowsCount: Int): Double {
        val tableFlow = flow as FlowWithHeightCalc<*>
        val tableFlowHeight = tableFlow.`get flow height based on rows count`(rowsCount)
        val tableBodyHeight = tableFlowHeight + snappedTopInset() + snappedBottomInset()
        return tableBodyHeight
    }
}
        
class FlowWithHeightCalc<CellItem: IndexedCell<*>> : VirtualFlow<CellItem>() {
    fun `get flow height based on rows count`(rowsCount: Int): Double {
        var flowHeight = 0.0
        // TODO get the row count from the flow itself?
        for (rowIndex in 0 until rowsCount) {
            val rowHeight = getCellLength(rowIndex)
            flowHeight += rowHeight
        }
        
        val isFlowEmpty: Boolean = flowHeight == 0.0
        if (isFlowEmpty) {
            val flowHeightDefault = 50.0
            flowHeight = flowHeightDefault
        }
        
        return flowHeight
    }
}
