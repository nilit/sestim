package io.gitlab.zalent.sestim.gui.view

import javafx.beans.property.SimpleBooleanProperty
import tornadofx.*

abstract class ViewDockerized : View() {
    val viewDocker: ViewDocker by inject()
    
    val controlButtons = ControlButtons()
    class ControlButtons {
        val isRefreshEnabled = SimpleBooleanProperty(false)
        val isSaveEnabled = SimpleBooleanProperty(false)
        val isDeleteEnabled = SimpleBooleanProperty(false)
        val isCreateEnabled = SimpleBooleanProperty(false)
    }
    
    fun configureControlButtons(
        isRefreshEnabled: Boolean = false,
        isSaveEnabled: Boolean = false,
        isDeleteEnabled: Boolean = false,
        isCreateEnabled: Boolean = false
    ) {
        controlButtons.isRefreshEnabled.value = isRefreshEnabled
        controlButtons.isSaveEnabled.value = isSaveEnabled
        controlButtons.isDeleteEnabled.value = isDeleteEnabled
        controlButtons.isCreateEnabled.value = isCreateEnabled
    }
}
