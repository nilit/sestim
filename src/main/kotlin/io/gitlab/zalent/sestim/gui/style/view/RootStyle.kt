package io.gitlab.zalent.sestim.gui.style.view

import io.gitlab.zalent.sestim.gui.style.NotoSans
import tornadofx.*

class RootStyle : Stylesheet() {
    
    init {
        root {
            backgroundColor += c("fff")
            font = NotoSans.regular
            fontSize = 13.px
        }
    }
}
