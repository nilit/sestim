package io.gitlab.zalent.sestim.gui.ident

import kotlin.reflect.KProperty

/**
 * Raw id to testfx selector formatter. Allows to write
 * ```
 *     clickOn(id[entityEstimateOwner.list.button.create]
 * ```
 * instead of
 * ```
 *     clickOn(id[entityEstimateOwner.list.button.create]
 * ```
 */
object id {
    operator fun get(cssId: CssId): JavaFxSelector = "#${cssId.name}"
}

/**
 * Allows to write
 * ```
 *     clickOn(cellId[EstimateModel::best, rowIndex]
 * ```
 * instead of
 * ```
 *     clickOn("#tablecell-${EstimateModel::best.name}-$rowIndex")
 * ```
 */
object cellId {
    operator fun get(columnProperty: KProperty<*>, rowIndex: Int): JavaFxSelector {
        return "#tablecell-${columnProperty.name}-$rowIndex"
    }
}

typealias JavaFxSelector = String
