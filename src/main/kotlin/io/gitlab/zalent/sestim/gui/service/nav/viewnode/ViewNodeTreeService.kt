package io.gitlab.zalent.sestim.gui.service.nav.viewnode

import io.gitlab.zalent.sestim.gui.view.project.ProjectCreateView
import io.gitlab.zalent.sestim.gui.view.project.ProjectEditView
import io.gitlab.zalent.sestim.gui.view.project.estimate.ProjectEstimateEditView
import io.gitlab.zalent.sestim.gui.view.project.ProjectListView
import io.gitlab.zalent.sestim.gui.view.project.story.StoryCreateView
import io.gitlab.zalent.sestim.gui.view.project.story.StoryEditView
import io.gitlab.zalent.sestim.gui.view.project.story.estimate.StoryEstimateEditView
import io.gitlab.zalent.sestim.gui.view.project.story.task.TaskCreateView
import io.gitlab.zalent.sestim.gui.view.project.story.task.TaskEditView
import io.gitlab.zalent.sestim.gui.view.project.story.task.estimate.TaskEstimateEditView
import org.springframework.stereotype.Service
import tornadofx.*
import javax.annotation.PostConstruct
import kotlin.reflect.KClass

/**
 * Stores the app's views tree. Especially useful for the breadcrumbs render.
 */
@Service
class ViewNodeTreeService {
    lateinit var rootnode: ViewNode
    
    @PostConstruct
    fun init() {
        rootnode = buildRootnode("Projects", ProjectListView::class) {        
            node("Create a project", ProjectCreateView::class)
            node("Edit a project", ProjectEditView::class) {
                node("Edit an estimate", ProjectEstimateEditView::class)
                
                node("Add a story", StoryCreateView::class)
                node("Edit a story", StoryEditView::class) {
                    node("Edit an estimate", StoryEstimateEditView::class)
                    
                    node("Add a task", TaskCreateView::class)
                    node("Edit a task", TaskEditView::class) {
                        node("Edit an estimate", TaskEstimateEditView::class)
                    }
                }
            }
        }
    }

    fun findNode(viewToFind: KClass<out View>, root: ViewNode = rootnode): ViewNode? {
        if (viewToFind == root.viewClass) {
            return root
        }
        
        val childFound: ViewNode? = root.children.find { viewToFind == it.viewClass }
        val isChildFound = childFound != null
        if (isChildFound) {
            return root.children.find { viewToFind == it.viewClass }!!
        }
        
        for (child in root.children) {
            val viewFound = findNode(viewToFind, child)
            if (viewFound != null) {
                return viewFound
            }
        }
        return null
    }
}
