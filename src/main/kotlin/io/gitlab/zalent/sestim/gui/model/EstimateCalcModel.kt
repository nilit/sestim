package io.gitlab.zalent.sestim.gui.model

import io.gitlab.zalent.sestim.database.entity.Estimate
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import java.time.format.DateTimeFormatter

class EstimateCalcModel : ItemViewModel<Estimate.Calc> {
    val id = bind(Estimate.Calc::id)
    
    val best = SimpleStringProperty()
    val worst = SimpleStringProperty()
    val expected = SimpleStringProperty()
    val actual = SimpleStringProperty()
    
    val dateCreated = SimpleStringProperty()

    constructor(item: Estimate.Calc? = null) {
        if (item != null) rebind { this.item = item }
    }
    
    init {
        initTimePropertiesConversion()
        initDateCreatedPropertyConversion()
    }
    
    private fun initTimePropertiesConversion() {
        this.itemProperty.onChange {
            best.value = item.best.toString()
            worst.value = item.worst.toString()
            expected.value = item.expected.toString()
            actual.value = item.actual.toString()
        }
    }
    
    private fun initDateCreatedPropertyConversion() {
        val dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm")
        this.itemProperty.onChange {
            dateCreated.value = item.dateCreated.format(dateFormatter)
        }
    }
}
