package io.gitlab.zalent.sestim.gui.controller

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.service.EstimateCalcService
import io.gitlab.zalent.sestim.domain.estimate.calc.EstimateCalcCreator
import io.gitlab.zalent.sestim.gui.model.EstimateCalcModel
import io.gitlab.zalent.sestim.gui.service.list.EstimateCalcListService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

@Controller
class EstimateCalcController {
    @Autowired private lateinit var calcService: EstimateCalcService
    @Autowired private lateinit var calcCreator: EstimateCalcCreator
    @Autowired private lateinit var calcListService: EstimateCalcListService

    fun create(parentEstimate: Estimate, estimateOwnerEntity: EstimateOwnerEntity) {
        val estimateCalc = calcCreator.create(parentEstimate, estimateOwnerEntity)
        calcListService.listProperty.add(EstimateCalcModel(estimateCalc))
    }
    
    fun delete(calcModel: EstimateCalcModel) {
        calcService.delete(calcModel.item)
        calcListService.listProperty.remove(calcModel)
    }
}
