package io.gitlab.zalent.sestim.gui.view.project.story.estimate

import io.gitlab.zalent.sestim.gui.view.estimate.EstimateListFragment

class StoryEstimateListFragment : EstimateListFragment() {
    override val estimateEditViewClass = StoryEstimateEditView::class
}
