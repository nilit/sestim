package io.gitlab.zalent.sestim.gui.view.project

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.control.extension.onMouseLeftButtonClickAtItem
import io.gitlab.zalent.sestim.gui.control.tableviewWithoutEmptyRows
import io.gitlab.zalent.sestim.gui.controller.ProjectController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.ProjectModel
import io.gitlab.zalent.sestim.gui.service.list.ProjectListService
import io.gitlab.zalent.sestim.gui.util.runAsyncWithIndicator
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.scene.control.TableView
import tornadofx.*

class ProjectListView : ViewDockerized() {    
    private val controller: ProjectController by di()
    private val projectListService: ProjectListService by di()
    
    override val root = vbox {
        id = ident.project.list.container.name
        
        tableviewWithoutEmptyRows(projectListService.listProperty) {
            id = ident.project.list.node.name
            
            column<ProjectModel, String>("name", ProjectModel::name) {
                minWidth = 225.0
            }
            column<ProjectModel, Project.State>("state", ProjectModel::state) {
                minWidth = 60.0
            }
            column<ProjectModel, Long>("KLOC", ProjectModel::KLOC) {
                minWidth = 60.0
            }
            
            contextmenu {
                id = ident.project.list.contextmenu.node.name
                item("edit") {
                    id = ident.project.list.contextmenu.edit.name
                    action { openProjectEditView(selectedItem!!) }
                }
                item("delete") {
                    id = ident.project.list.contextmenu.delete.name
                    action { deleteProject(selectedItem!!) }
                }
            }

            onMouseLeftButtonClickAtItem {
                openProjectEditView(selectedItem!!)
            }
            
            placeholder = label("no projects yet")

            columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
        }
    }
    
    override fun onDock() {
        configureControlButtons(isRefreshEnabled = true, isCreateEnabled = true)
    }

    override fun onCreate() {
        viewDocker.dock(ProjectCreateView::class)
    }

    override fun onRefresh() {
        runAsyncWithIndicator {
            projectListService.updateList()
        }
    }

    private fun openProjectEditView(projectModel: ProjectModel) {
        val params = mapOf(ProjectEditView::project.name to projectModel.item)
        viewDocker.dock(ProjectEditView::class, params)
    }
    
    private fun deleteProject(projectModel: ProjectModel) {
        confirmation(
            header = "This will also delete all the project's stories. Continue?"
        ) { buttonType ->
            val isConfirmed = buttonType.buttonData.isDefaultButton
            if (isConfirmed) {
                // TODO runAsyncWithIndicator { controller.delete(projectModel.item) }
                controller.delete(projectModel.item)
            }
        }
    }
}
