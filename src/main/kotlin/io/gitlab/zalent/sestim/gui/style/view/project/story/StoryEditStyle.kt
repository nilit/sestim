package io.gitlab.zalent.sestim.gui.style.view.project.story

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.style.stylesheet.box
import tornadofx.*

class StoryEditStyle : Stylesheet() {
    private val editForm by cssid(ident.story.edit.form.name)
    private val descFieldLabel by cssid(ident.story.edit.field.desc.label.name)
    private val descFieldArea by cssid(ident.story.edit.field.desc.textarea.name)
    
    init {
        editForm{
            fieldset {
                padding = box(bottom = 12)
            }
            descFieldLabel {
                padding = box(top = 5, bottom = 7)
            }
            descFieldArea {
                maxHeight = 130.px
                fontFamily = "monospace"
            }
        }
    }
}
