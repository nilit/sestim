package io.gitlab.zalent.sestim.gui.service.list

import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.service.EstimateService
import io.gitlab.zalent.sestim.gui.model.EstimateModel
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.annotation.PostConstruct

@Service
class EstimateListService {
    lateinit var listProperty: SimpleListProperty<EstimateModel>
    
    @Autowired private lateinit var estimateService: EstimateService
    
    @PostConstruct
    fun init() {
        // the ObservableList::setAll won't work without an initial
        // value (openjfx 8u60-b27-4)
        val listInitialValue = FXCollections.observableArrayList<EstimateModel>()
        listProperty = SimpleListProperty(listInitialValue)
    }
    
    fun updateList(estimateOwner: EstimateOwnerEntity) {
        val estimates = getListByEstimateOwner(estimateOwner)
        listProperty.setAll(estimates)
    }
    
    private fun getListByEstimateOwner(
        estimateOwnerEntity: EstimateOwnerEntity
    ): ObservableList<EstimateModel> {
        val entityList = estimateService.getByEstimateOwner(estimateOwnerEntity)
        val modelList = LinkedList<EstimateModel>()
        for (entity in entityList) {
            modelList.add(EstimateModel(entity))
        }
        return FXCollections.observableList(modelList)
    }
}
