package io.gitlab.zalent.sestim.gui.style.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import javafx.geometry.Pos
import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.paint.Color
import tornadofx.*

class DockControlPanelStyle : Stylesheet() {
    private val headerContainer by cssid(ident.header.container.name)
    
    private val buttonIcon by cssclass(classes.icon)
    private val buttonIconBack by cssid(ident.header.button.label.back.name)
    private val buttonIconRefresh by cssid(ident.header.button.label.refresh.name)
    private val buttonIconSave by cssid(ident.header.button.label.save.name)
    private val buttonIconCreate by cssid(ident.header.button.label.create.name)
    private val buttonIconDelete by cssid(ident.header.button.label.delete.name)
    
    object classes {
        val icon = "control-panel-button-icon"
    }

    init {
        headerContainer {
            minHeight = 35.px
            borderStyle += BorderStrokeStyle.SOLID
            borderWidth += box(top = 0.px, right = 0.px, left = 0.px, bottom = 1.px)
            borderColor += box(c("e1e4e8"))
            backgroundColor += c("#f2f2f2")
            
            val spaceBetweenButton = 5.px
            padding = box(vertical = spaceBetweenButton, horizontal = 6.px)
            spacing = spaceBetweenButton
            
            alignment = Pos.CENTER
            
            button {
                padding = box(5.px)
                backgroundInsets += box(0.px)
                backgroundRadius += box(2.px)
                backgroundColor += Color.TRANSPARENT
                
                and(hover) {
                    backgroundColor += c("#d7d7d7")
                }
                and(armed) {
                    backgroundColor += c("#d7d7d7")
                    backgroundRadius += box(20.px)
                }
            
                buttonIcon {
                    val width = 15.px
                    minWidth = width
                    minHeight = width
                    maxWidth = width
                    maxHeight = width
                    backgroundColor += c("#5a5a5a")
                    
                    and(buttonIconBack) { shape = "M20,11V13H8L13.5,18.5L12.08,19.92L4.16,12L12.08,4.08L13.5,5.5L8,11H20Z" }
                    and(buttonIconRefresh) { shape = "M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z" }
                    and(buttonIconSave) { shape = "M15,9H5V5H15M12,19A3,3 0 0,1 9,16A3,3 0 0,1 12,13A3,3 0 0,1 15,16A3,3 0 0,1 12,19M17,3H5C3.89,3 3,3.9 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V7L17,3Z" }
                    and(buttonIconCreate) { shape = "M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm5 11h-4v4h-2v-4H7v-2h4V7h2v4h4v2z" }
                    and(buttonIconDelete) { shape = "M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" }
                }
            }
        }
    }
}
