package io.gitlab.zalent.sestim.gui.control

import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import tornadofx.*

// a copy-past from tornadofx, because I have no desire to dig into
// functions with such names as `opcr` and `op`

fun EventTarget.numberfield(
    property: ObservableValue<Long>,
    op: (NumberField.() -> Unit)? = null
) = numberfield().apply {
    bind(property)
    op?.invoke(this)
}

fun EventTarget.numberfield(
    value: Long? = null,
    op: (NumberField.() -> Unit)? = null
) = opcr(this, NumberField().apply { if (value != null) text = value.toString() }, op)

class NumberField : TextFieldFilterable() {
    override fun isCharAllowed(char: Char): Boolean = char.isDigit() or (char == ',')
}
