@file:Suppress("UNCHECKED_CAST")

package io.gitlab.zalent.sestim.gui.model

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.view.project.ProjectListView
import javafx.beans.property.Property
import tornadofx.*

@Suppress("EqualsOrHashCode")
class ProjectModel : ItemViewModel<Project> {
    val name = bind(Project::name)
    val pubId = bind { item?.observable(Project::pubId) as Property<ID>? }
    val state = bind(Project::state)
    val type = bind { item?.observable(Project::type) as Property<Project.Type>? }
    val KLOC = bind { item?.observable(Project::KLOC) as Property<Long>? }

    constructor(item: Project? = null) {
        if (item != null) rebind { this.item = item }
    }
    
    /**
     * Needed to select an appropriate [ProjectModel] in [ProjectListView].
     */
    override fun equals(other: Any?): Boolean {
        return when (other) {
            is ProjectModel -> {
                val isIdEqual = other.item.id == this.item.id
                val isNameEqual = other.name.value == this.name.value
                isIdEqual and isNameEqual
            }
            else -> super.equals(other)
        }
    }
}
