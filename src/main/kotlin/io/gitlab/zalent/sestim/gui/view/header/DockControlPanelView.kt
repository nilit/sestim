package io.gitlab.zalent.sestim.gui.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.service.nav.HistoryService
import io.gitlab.zalent.sestim.gui.style.view.header.DockControlPanelStyle
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import tornadofx.*

class DockControlPanelView : View() {
    private val historyService: HistoryService by di()

    private val viewCurrentProp = historyService.viewCurrentProp
    private val viewCurrent: ViewDockerized by historyService.viewCurrentProp

    override val root = hbox {
        id = ident.header.container.name
        
        button {
            id = ident.header.button.back.name
            tooltip { text = "go back" }
            disableWhen(historyService.isAtRootViewProp)
            action { viewCurrent.viewDocker.navigateBack() }

            graphic = label {
                styleClass += DockControlPanelStyle.classes.icon
                id = ident.header.button.label.back.name
            }
            styleClass += DockControlPanelStyle.classes.icon
        }
        button {
            id = ident.header.button.refresh.name
            tooltip { text = "refresh" }
            viewCurrentProp.onNonNullAssignment {
                enableWhen(viewCurrent.controlButtons.isRefreshEnabled)
            }
            action { viewCurrent.onRefresh() }
            shortcut("F5")

            graphic = label {
                styleClass += DockControlPanelStyle.classes.icon
                id = ident.header.button.label.refresh.name
            }
        }

        add(AddressBarView::class)

        button {
            id = ident.header.button.delete.name
            tooltip { text = "delete" }
            viewCurrentProp.onNonNullAssignment {
                enableWhen(viewCurrent.controlButtons.isDeleteEnabled)
            }
            action { viewCurrent.onDelete() }

            graphic = label {
                styleClass += DockControlPanelStyle.classes.icon
                id = ident.header.button.label.delete.name
            }
            styleClass += DockControlPanelStyle.classes.icon
        }
        button {
            id = ident.header.button.create.name
            tooltip { text = "create" }
            viewCurrentProp.onNonNullAssignment {
                enableWhen(viewCurrent.controlButtons.isCreateEnabled)
            }
            action { viewCurrent.onCreate() }

            graphic = label {
                styleClass += DockControlPanelStyle.classes.icon
                id = ident.header.button.label.create.name
            }
            styleClass += DockControlPanelStyle.classes.icon
        }
        button {
            id = ident.header.button.save.name
            tooltip { text = "save" }
            viewCurrentProp.onNonNullAssignment {
                enableWhen(viewCurrent.controlButtons.isSaveEnabled)
            }
            action { viewCurrent.onSave() }
            shortcut("Ctrl+S")

            graphic = label {
                styleClass += DockControlPanelStyle.classes.icon
                id = ident.header.button.label.save.name
            }
            styleClass += DockControlPanelStyle.classes.icon
        }
    }
}
