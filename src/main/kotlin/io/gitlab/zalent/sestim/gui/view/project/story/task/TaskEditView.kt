package io.gitlab.zalent.sestim.gui.view.project.story.task

import io.gitlab.zalent.sestim.database.entity.Task
import io.gitlab.zalent.sestim.gui.view.estimate.EstimateListFragment
import io.gitlab.zalent.sestim.gui.view.header.BreadcrumbsView
import io.gitlab.zalent.sestim.gui.view.project.story.task.estimate.TaskEstimateListFragment
import javafx.event.EventTarget
import tornadofx.*

class TaskEditView : TaskEditViewBase() {
    val task: Task by param()
    
    private lateinit var estimateListFragment: EstimateListFragment
    private val breadcrumbsView: BreadcrumbsView by inject()

    override fun onDock() {
        `init taskModel`(task)
        `init EstimateListFragment`(task)
        
        breadcrumbsView.`bind view's breadcrumb name to stringProperty`(
            viewClass = this::class, stringProp = taskModel.name
        )
    }
    
    override fun EventTarget.insertCustomNodesToForm() {
        fieldset {
            estimateListFragment = TaskEstimateListFragment()
            add(estimateListFragment)
        }
    }
    
    private fun `init taskModel`(task: Task) {
        taskModel.rebind { item = task }
        storyField.selectionModel.select(task.story)
    }

    private fun `init EstimateListFragment`(task: Task) {
        estimateListFragment.estimateOwnerProperty.value = task
    }
}
