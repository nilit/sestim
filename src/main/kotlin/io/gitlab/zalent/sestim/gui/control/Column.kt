package io.gitlab.zalent.sestim.gui.control

import javafx.beans.value.ObservableValue
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.Callback
import tornadofx.*
import kotlin.reflect.KProperty1

/**
 * Allows to find this column with testfx by selectors like:
 * ```
 * "#tablecell-${EstimateModel::best.name}-$rowIndex"
 * ```
 */
fun <RowItem, CellItem : Any> TableView<RowItem>.columnWithId(
    property: KProperty1<RowItem, ObservableValue<CellItem>>,
    name: String? = null,
    initFn: (TableColumn<RowItem, CellItem>.() -> Unit)? = null
): TableColumn<RowItem, CellItem> {
    val column = TableColumn<RowItem, CellItem>(name ?: property.name)
    column.cellValueFactory = Callback { property.call(it.value) }
    addColumnInternal(column)
    initFn?.invoke(column)
    column.setCellFactory {
        TextFieldTableCellWithId(columnName = name ?: property.name)
    }
    return column
}

class TextFieldTableCellWithId<RowItem, CellItem>(
    private val columnName: String
) : TextFieldTableCell<RowItem, CellItem>() {
    
    override fun updateIndex(index: Int) {
        super.updateIndex(index)
        id = "tablecell-$columnName-$index"
    }
}
