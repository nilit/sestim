package io.gitlab.zalent.sestim.gui.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.scene.layout.Priority
import tornadofx.*

class AddressBarView : ViewDockerized() {
    override val root = hbox {
        id = ident.header.addressbar.container.name
        hgrow = Priority.ALWAYS
        
        add(BreadcrumbsView::class)
        add(ProgressIndicatorView::class)
    }
}
