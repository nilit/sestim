package io.gitlab.zalent.sestim.gui.control

import javafx.scene.control.TextField
import javafx.scene.input.KeyEvent

/**
 * Allows to filter an input's chars by [isCharAllowed] method override.
 */
abstract class TextFieldFilterable : TextField() {
    init {
        filterUserInput()
    }
    
    abstract fun isCharAllowed(char: Char): Boolean

    private fun filterUserInput() {
        this.addEventFilter(KeyEvent.KEY_TYPED) { keyEvent ->
            if (isInputNotAllowed(keyEvent)) {
                disregardInput(keyEvent)
            }
        }
    }

    private fun isInputNotAllowed(keyEvent: KeyEvent): Boolean {
        val charArray = keyEvent.character.toCharArray()
        val charTypedIndex = keyEvent.character.toCharArray().lastIndex
        val charTyped = charArray[charTypedIndex]
        val isNotAllowed: Boolean = !isCharAllowed(charTyped)
        return isNotAllowed
    }

    private fun disregardInput(keyEvent: KeyEvent) = keyEvent.consume()
}
