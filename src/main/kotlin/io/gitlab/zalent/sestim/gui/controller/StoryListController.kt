package io.gitlab.zalent.sestim.gui.controller

import io.gitlab.zalent.sestim.gui.model.StoryModel
import io.gitlab.zalent.sestim.database.service.StoryService
import io.gitlab.zalent.sestim.gui.service.list.StoryListService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

@Controller
class StoryListController {
    @Autowired private lateinit var storyService: StoryService
    @Autowired private lateinit var storyListService: StoryListService

    fun delete(storyModel: StoryModel) {
        storyService.delete(storyModel.item)
        storyListService.updateList(storyModel.item.project)
    }
}
