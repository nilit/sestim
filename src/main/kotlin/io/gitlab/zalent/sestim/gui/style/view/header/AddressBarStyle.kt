package io.gitlab.zalent.sestim.gui.style.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.style.stylesheet.box
import javafx.geometry.Pos
import tornadofx.*

class AddressBarStyle : Stylesheet() {
    private val container by cssid(ident.header.addressbar.container.name)
    
    init {
        container {
            borderWidth += box(1.px)
            borderColor += box(c("E2E2E2"))
            backgroundColor += c("fff")
            minHeight = 25.px
            padding = box(right = 5)
            alignment = Pos.CENTER_RIGHT
        }
    }
}
