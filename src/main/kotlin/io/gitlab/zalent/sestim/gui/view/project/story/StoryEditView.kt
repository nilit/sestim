package io.gitlab.zalent.sestim.gui.view.project.story

import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.gui.util.runAsyncWithIndicator
import io.gitlab.zalent.sestim.gui.view.estimate.EstimateListFragment
import io.gitlab.zalent.sestim.gui.view.header.BreadcrumbsView
import io.gitlab.zalent.sestim.gui.view.project.story.estimate.StoryEstimateListFragment
import io.gitlab.zalent.sestim.gui.view.project.story.task.TaskListView
import javafx.event.EventTarget
import tornadofx.*

class StoryEditView : StoryEditViewBase() {
    val story: Story by param()

    private lateinit var estimateListFragment: EstimateListFragment
    private val breadcrumbsView: BreadcrumbsView by inject()
    private val taskListView: TaskListView by inject()

    init {
        controlButtons.isRefreshEnabled.value = true
    }
    
    override fun onDock() {
        storyModel.rebind { item = story }
        `init EstimateListFragment`(story)
        `init TaskListView`(story)
        breadcrumbsView.`bind view's breadcrumb name to stringProperty`(
            viewClass = this::class, stringProp = storyModel.name
        )
    }
        
    override fun EventTarget.insertCustomNodesToStoryForm() {
        fieldset {
            estimateListFragment = StoryEstimateListFragment()
            add(estimateListFragment)
            add(TaskListView::class)
        }
    }

    override fun onRefresh() {
        runAsyncWithIndicator {
            estimateListFragment.onRefresh()
            taskListView.onRefresh()
        }
    }

    private fun `init EstimateListFragment`(story: Story) {
        estimateListFragment.estimateOwnerProperty.value = story
    }
    
    private fun `init TaskListView`(story: Story) {
        taskListView.storyProperty.value = story
    }
}
