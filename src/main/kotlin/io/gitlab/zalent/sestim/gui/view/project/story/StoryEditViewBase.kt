package io.gitlab.zalent.sestim.gui.view.project.story

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.gui.control.numberfield
import io.gitlab.zalent.sestim.gui.controller.StoryEditController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.StoryModel
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.event.EventTarget
import javafx.scene.control.ChoiceBox
import tornadofx.*

abstract class StoryEditViewBase : ViewDockerized() {
    protected val storyModel: StoryModel = StoryModel(Story())
    
    protected lateinit var projectField: ChoiceBox<Project>

    protected val controller: StoryEditController by di()

    override val root = form {
        id = ident.story.edit.form.name
        
        fieldset {
            field("Name") {
                textfield(storyModel.name) {
                    required()
                    id = ident.story.edit.field.name.name
                }
            }
            hbox(40) {
                vbox {
                    field("Project") {
                        choicebox(storyModel.project, values = controller.projectListProperty) {
                            id = ident.story.edit.field.project.name
                            projectField = this@choicebox
                        }
                    }
                    field("Pub ID") {
                        numberfield(storyModel.pubId) {
                            id = ident.story.edit.field.pubId.name
                        }
                    }
                    field("LOC") {
                        numberfield(storyModel.LOC) {
                            id = ident.story.edit.field.LOC.name
                        }
                    }
                }
                vbox {
                    field("Type") {
                        choicebox(storyModel.type, values = PlanItemEntity.Type.values().toList()) {
                            id = ident.story.edit.field.type.name
                        }
                    }
                    field("State") {
                        choicebox(storyModel.state, values = PlanItemEntity.State.values().toList()) {
                            id = ident.story.edit.field.state.name
                        }
                    }
                    field("Size") {
                        choicebox(storyModel.size, values = PlanItemEntity.Size.values().toList()) {
                            id = ident.story.edit.field.size.name
                        }
                    }
                }
            }
            field {
                vbox {
                    label("Description"){
                        id = ident.story.edit.field.desc.label.name
                    }
                    textarea(storyModel.description) {
                        id = ident.story.edit.field.desc.textarea.name
                    }
                }
            }
        }
        insertCustomNodesToStoryForm()
    }
    
    init {
        controlButtons.isSaveEnabled.bind(storyModel.valid)
    }

    override fun onSave() {
        controller.save(storyModel)
        viewDocker.navigateBack()
    }

    /** For the form's extension by the subclasses. */
    protected open fun EventTarget.insertCustomNodesToStoryForm() { }
}
