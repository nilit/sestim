package io.gitlab.zalent.sestim.gui.view.project.story.task

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.gui.control.numberfield
import io.gitlab.zalent.sestim.gui.controller.TaskController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.TaskModel
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.event.EventTarget
import javafx.scene.control.ChoiceBox
import tornadofx.*

abstract class TaskEditViewBase : ViewDockerized() {
    protected val taskModel: TaskModel = TaskModel()

    protected lateinit var storyField: ChoiceBox<Story>
    
    protected val controller: TaskController by di()
    
    override val root = form {
        id = ident.task.edit.form.name
        fieldset {
            field("Name") {
                textfield(taskModel.name) {
                    required()
                    id = ident.task.edit.field.name.name
                }
            }
            hbox(40) {
                vbox {
                    field("Story") {
                        choicebox(taskModel.story, values = controller.storyListProperty) {
                            id = ident.task.edit.field.story.name
                            storyField = this@choicebox
                        }
                    }
                    field("Pub ID") {
                        numberfield(taskModel.pubId) {
                            id = ident.task.edit.field.pubId.name
                        }
                    }
                    field("LOC") {
                        numberfield(taskModel.LOC) {
                            id = ident.task.edit.field.LOC.name
                        }
                    }
                }
                vbox {
                    field("State") {
                        choicebox(taskModel.state, values = PlanItemEntity.State.values().toList()) {
                            id = ident.task.edit.field.state.name
                        }
                    }
                    // TODO Task.Type here
                    field("Size") {
                        choicebox(taskModel.size, values = PlanItemEntity.Size.values().toList()) {
                            id = ident.task.edit.field.size.name
                        }
                    }
                }
            }
            field {
                vbox {
                    label("Description")
                    textarea(taskModel.description)
                }
            }
        }
        insertCustomNodesToForm()
    }
    
    init {
        controlButtons.isSaveEnabled.bind(taskModel.valid)
    }
    
    override fun onSave() {
        controller.save(taskModel)
        viewDocker.navigateBack()
    }
    
    /** For the form's extension by the subclasses. */
    protected open fun EventTarget.insertCustomNodesToForm() { }
}
