package io.gitlab.zalent.sestim.gui.style.stylesheet

import tornadofx.*

fun box(
    top: Int = 0, right: Int = 0, bottom: Int = 0, left: Int = 0
): CssBox<Dimension<Dimension.LinearUnits>> {
    return box(top.px, right.px, bottom.px, left.px)
}

fun box(vertical: Int = 0, horizontal: Int = 0) = box(vertical = vertical.px, horizontal = horizontal.px)

fun box(all: Int = 0) = box(all.px)
