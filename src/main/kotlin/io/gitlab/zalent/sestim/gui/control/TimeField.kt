package io.gitlab.zalent.sestim.gui.control

import io.gitlab.zalent.sestim.database.Time
import javafx.beans.property.Property
import javafx.scene.Node
import javafx.scene.control.TextField
import javafx.scene.control.TextFormatter
import javafx.util.StringConverter
import tornadofx.*
import java.util.regex.Pattern

fun Node.timefield(
    property: Property<Time>,
    initFn: (TimeField.() -> Unit)? = null
): TimeField {
    val timeField = TimeField()
    timeField.apply { 
        bind(property, converter = StringToTimeConverter())
    }
    return opcr(this, timeField, initFn)
}

class TimeField : TextField() {
    init {
        initInputValidator()
    }
    
    private fun initInputValidator() {
        textFormatter = TextFormatter<Time> { change ->
            val textNew = change.controlNewText
            val isValid = Regex("[0-9]{0,3}:[0-9]{0,2}").matches(textNew)
            if (isValid) {
                change
            } else {
                val changeReject = null
                changeReject
            }
        }
    }
}

class StringToTimeConverter : StringConverter<Time>() {
    override fun toString(time: Time) = time.toString()

    override fun fromString(string: String): Time {
        val pattern = Pattern.compile("(?<hours>[0-9]{0,3}):(?<minutes>[0-9]{0,2})")
        val matcher = pattern.matcher(string)
        matcher.find()
        
        val hoursStr: String = matcher.group("hours")
        val hours: Int = if (hoursStr != "") hoursStr.toInt() else 0
        
        val minutesStr: String = matcher.group("minutes")
        val minutes: Int = if (minutesStr != "") minutesStr.toInt() else 0
        
        return Time(hours = hours, minutes = minutes)
    }
}
