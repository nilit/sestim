package io.gitlab.zalent.sestim.gui.view.project.story.task

import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.gui.control.extension.onMouseLeftButtonClickAtItem
import io.gitlab.zalent.sestim.gui.control.tableviewWithoutEmptyRows
import io.gitlab.zalent.sestim.gui.controller.TaskController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.TaskModel
import io.gitlab.zalent.sestim.gui.property.ObjectPropertyNoisy
import io.gitlab.zalent.sestim.gui.service.list.TaskListService
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.scene.control.TableView
import javafx.scene.layout.Priority
import tornadofx.*

class TaskListView : ViewDockerized() {
    val storyProperty = ObjectPropertyNoisy<Story>()
    private var story: Story by storyProperty

    private val controller: TaskController by di()
    private val taskListService: TaskListService by di()

    override val root = vbox {
        id = ident.task.list.container.name
        
        hbox {
            id = ident.task.list.header.container.name

            label("Tasks") {
                id = ident.task.list.header.label.name
            }
            hbox { hgrow = Priority.ALWAYS }
            button("add a task") {
                id = ident.task.list.button.create.name
                action { openTaskCreateView(story = story) }
            }
        }
        
        tableviewWithoutEmptyRows(taskListService.listProperty) {
            id = ident.task.list.node.name
            column<TaskModel, String>("name", TaskModel::name) {
                minWidth = 300.0
            }
            column("state", TaskModel::state)
            
            contextmenu {
                id = ident.task.list.contextmenu.node.name
                item("edit") {
                    id = ident.task.list.contextmenu.edit.name
                    action { openTaskEditView(selectedItem!!) }
                }
                item("delete") {
                    id = ident.task.list.contextmenu.delete.name
                    action { controller.delete(selectedItem!!) }
                }
            }
            
            onMouseLeftButtonClickAtItem {
                openTaskEditView(selectedItem!!)
            }
            
            placeholder = label("no tasks yet")

            columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
        }
    }

    init {
        storyProperty.onNonNullAssignment { story ->
            taskListService.updateList(story)
        }
    }

    override fun onRefresh() {
        taskListService.updateList(story)
    }

    private fun openTaskCreateView(story: Story) {
        val params = mapOf(TaskCreateView::story.name to story)
        viewDocker.dock(TaskCreateView::class, params)
    }

    private fun openTaskEditView(taskModel: TaskModel) {
        val params = mapOf(TaskEditView::task.name to taskModel.item)
        viewDocker.dock(TaskEditView::class, params)
    }
}
