package io.gitlab.zalent.sestim.gui.controller

import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.service.TaskService
import io.gitlab.zalent.sestim.gui.model.StoryModel
import io.gitlab.zalent.sestim.gui.model.TaskModel
import io.gitlab.zalent.sestim.gui.service.list.StoryListService
import io.gitlab.zalent.sestim.gui.service.list.TaskListService
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import tornadofx.*
import javax.annotation.PostConstruct

@Controller
class TaskController {
    lateinit var storyListProperty: SimpleListProperty<Story>
    
    @Autowired private lateinit var taskService: TaskService
    @Autowired private lateinit var taskListService: TaskListService
    @Autowired private lateinit var storyListService: StoryListService
        
    @PostConstruct
    fun init() {
        initStoryListProperty()
    }
    
    fun save(taskModel: TaskModel) {
        val isMovedToAnotherStory: Boolean = taskModel.story.isDirty
        if (isMovedToAnotherStory) {
            val storyOld = taskModel.story.value
            saveTaskModel(taskModel)
            taskListService.updateList(storyOld)
        } else {
            saveTaskModel(taskModel)
            val storyNew = taskModel.item.story
            taskListService.updateList(storyNew)
        }
    }

    fun delete(taskModel: TaskModel) {
        taskService.delete(taskModel.item)
        taskListService.updateList(taskModel.item.story)
    }
    
    private fun saveTaskModel(taskModel: TaskModel) {
        taskModel.commit()
        taskService.save(taskModel.item)
    }
    
    private fun initStoryListProperty() {
        storyListProperty = `create storyListProperty`()
        `bind storyListProperty to StoryListService onChange`(storyListProperty)
    }
    
    private fun `create storyListProperty`(): SimpleListProperty<Story> {
        val projectList = storyListService.listProperty.map { it.item }
        val projectListObservable = FXCollections.observableList(projectList)
        return SimpleListProperty<Story>(projectListObservable)
    }
    
    private fun `bind storyListProperty to StoryListService onChange`(
        storyListProperty: SimpleListProperty<Story>
    ) {
        storyListService.listProperty.onChange<StoryModel> { listChange ->
            val storyList: List<Story> = listChange.list.map { it.item }
            storyListProperty.setAll(storyList)
        }
    }
}
