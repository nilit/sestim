package io.gitlab.zalent.sestim.gui.control

import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import tornadofx.*

fun EventTarget.floatfield(
    property: ObservableValue<Double>,
    op: (FloatField.() -> Unit)? = null
) = floatfield().apply {
    bind(property)
    op?.invoke(this)
}

fun EventTarget.floatfield(
    value: Double? = null,
    op: (FloatField.() -> Unit)? = null
) = opcr(this, FloatField().apply { if (value != null) text = value.toString() }, op)

class FloatField : TextFieldFilterable() {
    override fun isCharAllowed(char: Char): Boolean {
        return char.isDigit() or (char == '.') or (char == ',')
    }
}
