package io.gitlab.zalent.sestim.gui.view.project.story

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import tornadofx.*

class StoryCreateView : StoryEditViewBase() {
    val project: Project by param()

    override fun onDock() {
        storyModel.rebind { item = Story(project = this@StoryCreateView.project) }
        projectField.selectionModel.select(project)
    }
}
