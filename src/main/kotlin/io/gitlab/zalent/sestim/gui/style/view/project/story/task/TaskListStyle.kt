package io.gitlab.zalent.sestim.gui.style.view.project.story.task

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.style.stylesheet.box
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class TaskListStyle : Stylesheet() {
    private val container by cssid(ident.task.list.container.name)
    private val headerContainer by cssid(ident.task.list.header.container.name)
    
    init {
        container {
            padding = box(top = 15)
            headerContainer {
                alignment = Pos.CENTER_LEFT
                padding = box(top = 11)
                borderWidth += box(top = 1)
                borderColor += box(
                    top = c("DDDDDD"),
                    right = Color.TRANSPARENT,
                    left = Color.TRANSPARENT,
                    bottom = Color.TRANSPARENT
                )
            }
        }
    }
}
