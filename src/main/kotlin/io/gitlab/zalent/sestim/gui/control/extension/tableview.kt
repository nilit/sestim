package io.gitlab.zalent.sestim.gui.control.extension

import javafx.scene.control.TableView
import javafx.scene.input.InputEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import tornadofx.*

fun <T> TableView<T>.onMouseLeftButtonClickAtItem(action: (T) -> Unit) {
    val isItemSelected = { event: InputEvent ->
        event.target.isInsideRow() and !selectionModel.isEmpty
    }

    addEventFilter(MouseEvent.MOUSE_CLICKED) { event: MouseEvent ->
        if ((event.button == MouseButton.PRIMARY) and isItemSelected(event)) {
            action(selectedItem!!)
        }
    }
}
