package io.gitlab.zalent.sestim.gui.style.stylesheet

import tornadofx.*

/**
 * Javafx class names seem completely screwed up to me, here I tried to make
 * them more reasonable.
 */
open class TableStylesheet : Stylesheet() {
    val tableHeaderRow by cssclass("column-header-background")
    val tableHeaderRowFiller by cssclass("filler")
    val tableHeaderColumns by cssclass("nested-column-header")
    val tableHeaderColumn by cssclass("column-header")
    
    val tableRowContainer by cssclass("sheet")
    val tableRow by cssclass("table-row-cell")
}
