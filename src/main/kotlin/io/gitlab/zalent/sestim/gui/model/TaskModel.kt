package io.gitlab.zalent.sestim.gui.model

import io.gitlab.zalent.sestim.database.entity.Task
import tornadofx.*

class TaskModel : ItemViewModel<Task> {
    val id = bind(Task::id)
    val name = bind(Task::name)
    val description = bind(Task::description)
    val story = bind(Task::story)
    val pubId = bind(Task::pubId)
    val state = bind(Task::state)
    val size = bind(Task::size)
    val LOC = bind(Task::LOC)
    
    constructor(item: Task? = null) {
        if (item != null) rebind { this.item = item }
    }
}
