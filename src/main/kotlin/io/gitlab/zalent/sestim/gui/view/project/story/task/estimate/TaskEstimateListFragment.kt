package io.gitlab.zalent.sestim.gui.view.project.story.task.estimate

import io.gitlab.zalent.sestim.gui.view.estimate.EstimateListFragment

// TODO hide EstimateCalcListFragment from Task
class TaskEstimateListFragment : EstimateListFragment() {
    override val estimateEditViewClass = TaskEstimateEditView::class
}
