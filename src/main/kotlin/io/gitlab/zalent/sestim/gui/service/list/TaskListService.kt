package io.gitlab.zalent.sestim.gui.service.list

import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.service.TaskService
import io.gitlab.zalent.sestim.gui.model.TaskModel
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.annotation.PostConstruct

@Service
class TaskListService {
    lateinit var listProperty: SimpleListProperty<TaskModel>
    
    @Autowired private lateinit var taskService: TaskService

    @PostConstruct
    fun init() {
        val listInitialValue = FXCollections.observableArrayList<TaskModel>()
        listProperty = SimpleListProperty(listInitialValue)
    }
    
    fun updateList(story: Story) {
        val stories = getTaskListByStory(story)
        listProperty.setAll(stories)
    }
    
    private fun getTaskListByStory(story: Story): ObservableList<TaskModel> {
        val taskList = taskService.getByStory(story)
        val taskModelList = LinkedList<TaskModel>()
        for (task in taskList) {
            val taskModel = TaskModel(task)
            taskModelList.add(taskModel)
        }
        return FXCollections.observableList(taskModelList)
    }
}
