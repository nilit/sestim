package io.gitlab.zalent.sestim.gui.style.view.project.estimate

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.style.stylesheet.box
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class EstimateListStyle : Stylesheet() {
    private val tableList by cssid(ident.estimate.list.node.name)
    private val headerContainer by cssid(ident.estimate.list.header.container.name)
    
    init {
        headerContainer {
            alignment = Pos.CENTER_LEFT
            padding = box(top = 11, bottom = 7)
            borderWidth += box(top = 1)
            borderColor += box(
                top = c("DDDDDD"),
                right = Color.TRANSPARENT,
                left = Color.TRANSPARENT,
                bottom = Color.TRANSPARENT
            )
        }
        tableList {
            fontSize = 13.px
            tableCell {
                textField {
                    padding = box(0)
                }
            }
        }
    }
}
