package io.gitlab.zalent.sestim.gui.view.header

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.style.view.header.ProgressIndicatorStyle
import javafx.scene.control.ProgressIndicator
import tornadofx.*

class ProgressIndicatorView : View() {
    override val root = progressindicator {
        id = ident.header.addressbar.progressIndicator.name
    }
    
    private val indicator: ProgressIndicator = root
    private val visiblePseudoClass = ProgressIndicatorStyle.pseudoClasses.visible
    
    fun start() {
        indicator.addPseudoClass(visiblePseudoClass)
    }
    
    fun stop() {
        runAsync {
            Thread.sleep(100)
            indicator.removePseudoClass(visiblePseudoClass)
        }
    }
}
