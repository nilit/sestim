package io.gitlab.zalent.sestim.gui.view.project

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.util.runAsyncWithIndicator
import io.gitlab.zalent.sestim.gui.view.estimate.EstimateListFragment
import io.gitlab.zalent.sestim.gui.view.header.BreadcrumbsView
import io.gitlab.zalent.sestim.gui.view.project.estimate.ProjectEstimateListFragment
import io.gitlab.zalent.sestim.gui.view.project.story.StoryListView
import javafx.scene.Node
import tornadofx.*

class ProjectEditView : ProjectEditViewBase() {
    val project: Project by param()
    
    private lateinit var estimateListFragment: EstimateListFragment

    private val storyListView: StoryListView by inject()
    private val breadcrumbsView: BreadcrumbsView by inject()

    init {
        controlButtons.isRefreshEnabled.value = true
    }

    override fun onDock() {
        projectModel.rebind { item = project }
        `init EstimateListFragment`(project)
        `init StoryListView`(project)
        `set current breadcrumb name to projectModel's name`()
    }

    override fun onRefresh() {
        runAsyncWithIndicator {
            projectModel.rollback()
            storyListView.onRefresh()
            estimateListFragment.onRefresh()
        }
    }

    override fun Node.insertCustomNodesIntoForm() {
        estimateListFragment = ProjectEstimateListFragment()
        add(estimateListFragment)
        add(StoryListView::class)
    }

    private fun `init EstimateListFragment`(project: Project) {
        estimateListFragment.estimateOwnerProperty.value = project
    }

    private fun `init StoryListView`(project: Project) {
        storyListView.projectProperty.value = project
    }
    
    private fun `set current breadcrumb name to projectModel's name`() {
        breadcrumbsView.`bind view's breadcrumb name to stringProperty`(
            viewClass = ProjectEditView::class,
            stringProp = projectModel.name
        )
    }
}
