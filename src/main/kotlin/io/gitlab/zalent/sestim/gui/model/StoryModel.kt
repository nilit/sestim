package io.gitlab.zalent.sestim.gui.model

import io.gitlab.zalent.sestim.database.entity.Story
import tornadofx.*

class StoryModel : ItemViewModel<Story> {
    val id = bind(Story::id)
    val project = bind(Story::project)
    val name = bind(Story::name)
    val description = bind(Story::description)
    val pubId = bind(Story::pubId)
    val type = bind(Story::type)
    val state = bind(Story::state)
    val size = bind(Story::size)
    val LOC = bind(Story::LOC)
    
    constructor(item: Story? = null) {
        if (item != null) rebind { this.item = item }
    }
}
