package io.gitlab.zalent.sestim.gui.model

import io.gitlab.zalent.sestim.database.entity.Estimate
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import java.time.format.DateTimeFormatter

class EstimateModel : ItemViewModel<Estimate> {
    val id = bind(Estimate::id)
    
    val best = bind(Estimate::best)
    val worst = bind(Estimate::worst)
    val expected = bind(Estimate::expected)
    val actual = bind(Estimate::actual)
    
    val bestString = SimpleStringProperty()
    val worstString = SimpleStringProperty()
    val expectedString = SimpleStringProperty()
    val actualString = SimpleStringProperty()
    
    val isDateValid = bind(Estimate::isDateValid)
    val isAdjusted = bind(Estimate::isAdjusted)
    
    val dateCreated = SimpleStringProperty()
    val dateModified = SimpleStringProperty()

    constructor(item: Estimate) {
        rebind { this.item = item }
    }
    
    init {
        initTimePropertiesConversion()
        initDatePropertiesConversion()
    }

    private fun initTimePropertiesConversion() {
        this.itemProperty.onChange {
            bestString.value = item.best.toString()
            worstString.value = item.worst.toString()
            expectedString.value = item.expected.toString()
            actualString.value = item.actual.toString()
        }
    }
    
    private fun initDatePropertiesConversion() {
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm")
        this.itemProperty.onChange {
            dateCreated.value = item.dateCreated.format(formatter)
            dateModified.value = item.dateModified.format(formatter)
        }
    }
}
