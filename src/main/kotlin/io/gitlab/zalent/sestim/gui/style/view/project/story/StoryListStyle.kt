package io.gitlab.zalent.sestim.gui.style.view.project.story

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.style.stylesheet.box
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class StoryListStyle : Stylesheet() {
    private val container by cssid(ident.story.list.container.name)
    private val headerContainer by cssid(ident.story.list.header.container.name)
    private val buttonAdd by cssid(ident.story.list.button.add.name)
    private val listTable by cssid(ident.story.list.node.name)
    
    init {
        container {
            padding = box(top = 20)
            headerContainer {
                alignment = Pos.CENTER_LEFT
                padding = box(top = 11)
                borderWidth += box(top = 1)
                borderColor += box(
                    top = c("DDDDDD"),
                    right = Color.TRANSPARENT,
                    left = Color.TRANSPARENT,
                    bottom = Color.TRANSPARENT
                )
                
                buttonAdd {
                }
            }
            listTable {
                fontSize = 13.px
            }
        }
    }
}
