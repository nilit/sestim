package io.gitlab.zalent.sestim.gui.ident

/**
 * It wasn't divided on subpackages because kotlin compiles those objects
 * to java bytecode, which sometimes can't handle an indirect reference of
 * those values.
 */
object ident {

    object header {
        val container = CssId("top-container")
    
        object button {
            val refresh = CssId("header-refresh-button")
            val back = CssId("header-back-button")
            val create = CssId("header-create-button")
            val delete = CssId("header-delete-button")
            val save = CssId("header-save-button")
            object label {
                val refresh = CssId("header-refresh-button-label")
                val back = CssId("header-back-button-label")
                val create = CssId("header-create-button-label")
                val delete = CssId("header-delete-button-label")
                val save = CssId("header-save-button-label")
            }
        }
        object addressbar {
            val container = CssId("addressbar-container")
            val progressIndicator = CssId("addressbar-loading-indicator")
            object breadcrumbs {
                val container = CssId("breadcrumbs-container")
            }
        }
    }
    
    object project {
        object list {
            val container = CssId("project-list-container")
            val node = CssId("project-list-node")
            object contextmenu {
                val node = CssId("project-menu-node")
                val edit = CssId("project-edit-menu-action")
                val delete = CssId("project-delete-menu-action")
            }
        }
        object edit {
            val form = CssId("project-edit-form")
            
            object field {
                val name = CssId("project-name-field")
                val type = CssId("project-type-choicebox")
                val state = CssId("project-state-choicebox")
                val pubId = CssId("project-pubId-field")
                val KLOC = CssId("project-KLOC-field")
            }
        }
    }
    
    object story {
        object list {
            val container = CssId("story-list-container")
            val node = CssId("story-list-node")
            
            object header {
                val container = CssId("story-list-header-container")
                val label = CssId("story-list-header-name")
            }
            
            object contextmenu {
                val node = CssId("story-contextmenu-node")
                val edit = CssId("story-contextmenu-item-create")
                val delete = CssId("story-contextmenu-item-delete")
            }
            
            object button {
                val add = CssId("story-create-button")
            }
        }
        object edit {
            val form = CssId("story-edit-form")
            object field {
                val name = CssId("story-name-field")
                object desc {
                    val label = CssId("story-desc-label")
                    val textarea = CssId("story-desc-textarea")
                }
                val type = CssId("story-type-choicebox")
                val state = CssId("story-state-choicebox")
                val pubId = CssId("story-pubId-field")
                val project = CssId("story-project-field")
                val size = CssId("story-size-field")
                val LOC = CssId("story-LOC-field")
            }
        }
    }

    object task {
        object list {
            val container = CssId("task-list-container")
            object header {
                val container = CssId("task-list-header-container")
                val label = CssId("task-list-header-label")
            }
            val node = CssId("task-list-table")
            object contextmenu {
                val node = CssId("task-list-contextmenu")
                val edit = CssId("task-list-contextmenu-edit-item")
                val delete = CssId("task-list-contextmenu-delete-item")
            }
            object button {
                val create = CssId("task-create-button")
            }
        }
        object edit {
            val form = CssId("task-form")
            object field {
                val name  = CssId("task-name-field")
                val desc  = CssId("task-desc-field")
                val pubId = CssId("task-pubId-field")
                val state = CssId("task-state-choicebox")
                val story = CssId("task-project-field")
                val size = CssId("task-size-field")
                val LOC = CssId("task-LOC-field")
            }
        }
    }

    object estimate {
        object list {
            object header {
                val container = CssId("estimate-list-header-container")
                val label = CssId("estimate-list-header-label")
            }
            val node = CssId("estimate-list-table")
            object button {
                val add = CssId("estimate-add-button")
            }
            object contextmenu {
                val node = CssId("estimate-contextmenu-node")
                val delete = CssId("estimate-contextmenu-delete-action")
                val edit = CssId("estimate-contextmenu-edit-action")
            }
        }
        object edit {
            val form = CssId("estimate-edit-form")
            object field {
                val best = CssId("estimate-field-best")
                val worst = CssId("estimate-field-worst")
                val expected = CssId("estimate-field-expected")
                val actual = CssId("estimate-field-actual")
                
                val isDateValid = CssId("estimate-field-is-date-valid")
            }
        }
        object calcList {
            object header {
                val container = CssId("estimate-children-calc-list-header-container")
                val label = CssId("estimate-children-calc-list-header-label")
            }
            val node = CssId("estimate-children-calc-list-table")
            object button {
                val calculate = CssId("estimate-children-calc-button")
            }
            object contextmenu {
                val node = CssId("estimate-children-calc-contextmenu-node")
                val delete = CssId("estimate-children-calc-contextmenu-delete-action")
            }
        }
    }
}
