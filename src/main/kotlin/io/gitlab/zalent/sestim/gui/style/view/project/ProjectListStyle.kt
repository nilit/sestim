package io.gitlab.zalent.sestim.gui.style.view.project

import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.style.stylesheet.TableStylesheet
import io.gitlab.zalent.sestim.gui.style.stylesheet.box
import tornadofx.*

class ProjectListStyle : TableStylesheet() {
    private val projectListContainer by cssid(ident.project.list.container.name)

    init {
        projectListContainer {
            padding = box(top = 2, right = 20, left = 20, bottom = 2)
            
            tableView {
                fontSize = 14.px
    
                tableColumn {
                    padding = box(top = 11, right = 10, left = 10, bottom = 15)
                }
    
                tableHeaderRow {
                    tableHeaderColumns {
                        tableHeaderColumn {
                            label {
                                padding = box(top = 11, bottom = 12)
                                fontSize = 14.px
                                textFill = c("#929292")
                            }
                            borderWidth += box(bottom = 2)
                        }
                    }
                }
            }
        }
    }
}
