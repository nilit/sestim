package io.gitlab.zalent.sestim.gui.service.list

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.model.StoryModel
import io.gitlab.zalent.sestim.database.service.StoryService
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.annotation.PostConstruct

@Service
class StoryListService {
    lateinit var listProperty: SimpleListProperty<StoryModel>
    
    @Autowired private lateinit var storyService: StoryService

    @PostConstruct
    fun init() {
        initList()
    }
    
    fun updateList(project: Project) {
        val storyList = getStoryListByProject(project)
        listProperty.setAll(storyList)
    }

    private fun initList() {
        val listInitialValue = FXCollections.observableArrayList<StoryModel>()
        listProperty = SimpleListProperty(listInitialValue)
    }
    
    private fun getStoryListByProject(project: Project): ObservableList<StoryModel> {
        val storyList = storyService.getByProject(project)
        val storyModelList = LinkedList<StoryModel>()
        for (story in storyList) {
            val storyModel = StoryModel(story)
            storyModelList.add(storyModel)
        }
        return FXCollections.observableList(storyModelList)
    }
}
