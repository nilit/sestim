package io.gitlab.zalent.sestim.gui.style

import tornadofx.*

private val defaultSize = 11

object NotoSans {
    val regular = loadFont("/fonts/noto-sans~regular.ttf", defaultSize)!!
}
