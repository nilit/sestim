package io.gitlab.zalent.sestim.gui.view.project

import io.gitlab.zalent.sestim.database.entity.Project
import tornadofx.*

class ProjectCreateView : ProjectEditViewBase() {
    override fun onDock() {
        projectModel.rebind { item = Project() }
    }
}
