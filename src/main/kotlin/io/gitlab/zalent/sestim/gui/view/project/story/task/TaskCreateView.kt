package io.gitlab.zalent.sestim.gui.view.project.story.task

import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.entity.Task
import tornadofx.*

class TaskCreateView : TaskEditViewBase() {
    val story: Story by param()
    
    override fun onDock() {
        taskModel.rebind { item = Task(story = this@TaskCreateView.story) }
        storyField.selectionModel.select(story)
    }
}
