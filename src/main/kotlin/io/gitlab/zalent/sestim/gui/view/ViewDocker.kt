package io.gitlab.zalent.sestim.gui.view

import io.gitlab.zalent.sestim.gui.service.nav.HistoryService
import io.gitlab.zalent.sestim.gui.style.configureAppSize
import io.gitlab.zalent.sestim.gui.style.importAppStylesheets
import io.gitlab.zalent.sestim.gui.view.header.DockControlPanelView
import io.gitlab.zalent.sestim.gui.view.project.ProjectListView
import javafx.scene.layout.StackPane
import tornadofx.*
import kotlin.reflect.KClass

/**
 * This class replaces [Workspace] because it's practically not extensible:
 * - impossible to override [root] in order to insert any kind of new nodes
 * - it has a low cohesion at the class and method levels
 * - it can break testfx and it's hard to say why,
 * see the [issue](https://github.com/edvin/tornadofx/issues/500)
 */
class ViewDocker : View("sestim") {
    private var dockContainer: StackPane by singleAssign()

    private val viewRoot: ProjectListView by inject()

    private val historyService: HistoryService by di()
    
    override val root = borderpane {
        top {
            add(DockControlPanelView::class)
        }
        center {
            dockContainer = stackpane()
        }
    }
    
    init {
        initStyles()
        initRootView()
    }
    
    fun dock(viewNew: KClass<out View>, params: Map<String, Any>? = null) {
        historyService.pushView(viewNew, params)
        val viewNewParametrized = find(type = viewNew, params = params)
        dockViewIntoContainer(viewNewParametrized)
    }
    
    // TODO rename?
    fun navigateBack() {
        val viewPrevious = historyService.popView()
        dockViewIntoContainer(viewPrevious)
    }

    private fun initStyles() {
        configureAppSize()
        importAppStylesheets()
    }
    
    private fun initRootView() {
        dock(viewRoot::class)
    }

    private fun dockViewIntoContainer(viewNew: View) {
        dockContainer.clear()
        dockContainer += viewNew
    }
}
