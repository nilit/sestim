package io.gitlab.zalent.sestim.gui.service.nav.viewnode

import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import kotlin.reflect.KClass

class ViewNode {
    val nameProperty = SimpleStringProperty()
    var name: String by nameProperty
    
    val viewClass: KClass<out ViewDockerized>
    var parent: ViewNode? = null
    val children = arrayListOf<ViewNode>()
    var params = HashMap<String, Any>()
    
    constructor(name: String, viewClass: KClass<out ViewDockerized>) {
        this.name = name
        this.viewClass = viewClass
    }
}
