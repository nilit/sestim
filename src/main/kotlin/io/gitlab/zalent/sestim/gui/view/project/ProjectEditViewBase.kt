package io.gitlab.zalent.sestim.gui.view.project

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.gui.control.numberfield
import io.gitlab.zalent.sestim.gui.controller.ProjectController
import io.gitlab.zalent.sestim.gui.ident.ident
import io.gitlab.zalent.sestim.gui.model.ProjectModel
import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import javafx.scene.Node
import tornadofx.*

abstract class ProjectEditViewBase : ViewDockerized() {
    protected val projectModel = ProjectModel()
    
    protected val controller: ProjectController by di()

    override val root = form {
        id = ident.project.edit.form.name
        
        fieldset {
            hbox(40) {
                vbox {
                    field("Name") {
                        textfield(projectModel.name) {
                            required()
                            id = ident.project.edit.field.name.name
                        }
                    }
                    field("KLOC") {
                        numberfield(projectModel.KLOC) {
                            id = ident.project.edit.field.KLOC.name
                        }
                    }
                    field("Pub ID") {
                        numberfield(projectModel.pubId) {
                            id = ident.project.edit.field.pubId.name
                        }
                    }
                }
                vbox {
                    field("State") {
                        choicebox(projectModel.state, values = Project.State.values().toList()) {
                            id = ident.project.edit.field.state.name
                        }
                    }
                }
            }
        }
        insertCustomNodesIntoForm()
    }
    
    init {
        controlButtons.isSaveEnabled.value = true
    }

    override fun onSave() {
        projectModel.commit()
        controller.save(projectModel.item)
        viewDocker.dock(ProjectListView::class)
    }
    
    /** For the form's extension by its subclasses. */
    protected open fun Node.insertCustomNodesIntoForm() { }
}
