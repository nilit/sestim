package io.gitlab.zalent.sestim.gui.service.list

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.service.EstimateCalcService
import io.gitlab.zalent.sestim.gui.model.EstimateCalcModel
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.annotation.PostConstruct

@Service
class EstimateCalcListService {
    lateinit var listProperty: SimpleListProperty<EstimateCalcModel>

    @Autowired private lateinit var calcService: EstimateCalcService
        
    @PostConstruct
    fun init() {
        val listInitialValue = FXCollections.observableArrayList<EstimateCalcModel>()
        listProperty = SimpleListProperty(listInitialValue)
    }
    
    fun updateList(parentEstimate: Estimate) {
        val childrenCalcList = getListByEstimate(parentEstimate)
        listProperty.setAll(childrenCalcList)
    }
    
    private fun getListByEstimate(parentEstimate: Estimate): ObservableList<EstimateCalcModel> {
        val entityList = calcService.getByEstimate(parentEstimate)
        val modelList = LinkedList<EstimateCalcModel>()
        for (entity in entityList) {
            val model = EstimateCalcModel(entity)
            modelList.add(model)
        }
        return FXCollections.observableList(modelList)
    }
}
