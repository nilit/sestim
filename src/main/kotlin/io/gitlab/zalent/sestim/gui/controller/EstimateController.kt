package io.gitlab.zalent.sestim.gui.controller

import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.service.EstimateService
import io.gitlab.zalent.sestim.gui.model.EstimateModel
import io.gitlab.zalent.sestim.gui.service.list.EstimateListService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import tornadofx.*

@Controller
class EstimateController {
    @Autowired private lateinit var estimateService: EstimateService
    @Autowired private lateinit var estimateListService: EstimateListService
    
    fun create(ownerEntity: EstimateOwnerEntity): EstimateModel {
        val estimate = estimateService.create(ownerEntity)
        val estimateModel = EstimateModel(estimate)
        estimateListService.listProperty.add(estimateModel)
        return estimateModel
    }

    fun save(estimateModel: EstimateModel) {
        estimateModel.commit()
        estimateService.save(estimateModel.item)
        estimateModel.rebind { item = estimateModel.item }
    }
    
    fun delete(estimateModel: EstimateModel) {
        estimateService.delete(estimateModel.item)
        estimateListService.listProperty.remove(estimateModel)
    }
}
