package io.gitlab.zalent.sestim.gui.controller

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.service.ProjectService
import io.gitlab.zalent.sestim.gui.service.list.ProjectListService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

@Controller
class ProjectController {
    @Autowired private lateinit var projectService: ProjectService
    @Autowired private lateinit var projectListService: ProjectListService

    fun save(project: Project) {
        projectService.save(project)
        projectListService.updateList()
    }
    
    fun delete(project: Project) {
        projectService.delete(project)
        projectListService.updateList()
    }
}
