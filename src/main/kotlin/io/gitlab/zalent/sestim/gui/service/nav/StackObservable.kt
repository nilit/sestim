package io.gitlab.zalent.sestim.gui.service.nav

import java.util.*

class StackObservable<E>(private var stack: Stack<E>) {
    private val listeners = LinkedList<Listener>()
    
    fun onChange(listener: Listener) {
        listeners.add(listener)
    }
    
    fun push(elem: E): E {
        stack.push(elem)
        fireOnChangeEvents()
        return elem
    }
    
    fun pop(): E {
        val stackLast = stack.pop()
        fireOnChangeEvents()
        return stackLast
    }
    
    fun peek(): E = stack.peek()
    
    operator fun get(index: Int): E? = stack[index]
    
    private fun fireOnChangeEvents() {
        listeners.forEach { listener ->
            listener()
        }
    }
}

typealias Listener = () -> Unit
