package io.gitlab.zalent.sestim.gui.service.nav.viewnode

import io.gitlab.zalent.sestim.gui.view.ViewDockerized
import kotlin.reflect.KClass

internal fun buildRootnode(
    name: String,
    viewClass: KClass<out ViewDockerized>,
    init: ViewNode.() -> Unit
): ViewNode {
    val node = ViewNode(name, viewClass)
    node.init()
    return node
}

internal fun ViewNode.node(
    name: String,
    viewClass: KClass<out ViewDockerized>,
    init: (ViewNode.() -> Unit)? = null
): ViewNode {
    val node = ViewNode(name, viewClass)
    node.parent = this
    if (init != null) node.init()
    children.add(node)
    return node
}
