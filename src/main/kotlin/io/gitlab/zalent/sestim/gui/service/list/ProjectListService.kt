package io.gitlab.zalent.sestim.gui.service.list

import io.gitlab.zalent.sestim.gui.model.ProjectModel
import io.gitlab.zalent.sestim.database.service.ProjectService
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class ProjectListService {
    @Autowired private lateinit var projectService: ProjectService
    
    lateinit var listProperty: SimpleListProperty<ProjectModel>

    @PostConstruct
    fun init() {
        listProperty = SimpleListProperty(`get ProjectModel list as Observable`())
    }

    fun updateList() {
        listProperty.setAll(`get ProjectModel list as Observable`())
    }
    
    private fun `get ProjectModel list as Observable`(): ObservableList<ProjectModel> {
        val entityModelList = projectService.getAll().map {
            entity -> ProjectModel(entity)
        }
        return FXCollections.observableList(entityModelList)
    }
}
