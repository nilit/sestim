package io.gitlab.zalent.sestim.database.entity.base

import io.gitlab.zalent.sestim.database.ID

/**
 * Allows to apply polymorphism to the [Estimate]'s views and tests.
 */
interface EstimateOwnerEntity {
    var id: ID?
}
