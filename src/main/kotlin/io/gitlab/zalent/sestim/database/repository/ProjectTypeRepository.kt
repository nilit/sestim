package io.gitlab.zalent.sestim.database.repository

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.Project
import org.springframework.data.jpa.repository.JpaRepository

interface ProjectTypeRepository : JpaRepository<Project.Type, ID>
