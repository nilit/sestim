package io.gitlab.zalent.sestim.database.repository

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.entity.Task
import org.springframework.data.repository.CrudRepository

interface TaskRepository : CrudRepository<Task, ID> {
    fun findByStory(story: Story): List<Task>
}
