package io.gitlab.zalent.sestim.database.entity.base

// TODO make the fields like `ReadyForDev` more readable
// TODO rename: PlannedEntity, PlanTaskItem, PlannedItem, ScheduledItem, ...
abstract class PlanItemEntity : ClosableEntity {
    abstract var state: State
    
    val isNotActive: Boolean
        get() = (this.state == State.Completed) or (this.state == State.Dropped)
    
    enum class Type {
        NotSet,
        Feature,
        Chore,
        Bug,
    }
    
    enum class Size {
        NotSet,
        VerySmall,
        Small,
        Medium,
        Large,
        VeryLarge,
    }
    
    enum class State {
        Unscheduled,
        ReadyForDev,
        InDev,
//        ReadyForDev { override fun toString() = "Ready for dev" },
//        InDev { override fun toString() = "In development" },
        Completed,
        Dropped,
    }
}
