package io.gitlab.zalent.sestim.database.entity.base

import java.time.LocalDateTime

interface DatedEntity {
    val dateCreated: LocalDateTime
    var dateModified: LocalDateTime
}
