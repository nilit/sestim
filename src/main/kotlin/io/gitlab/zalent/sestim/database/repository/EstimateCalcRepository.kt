package io.gitlab.zalent.sestim.database.repository

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.Estimate
import org.springframework.data.jpa.repository.JpaRepository

interface EstimateCalcRepository : JpaRepository<Estimate.Calc, ID> {
    fun getByEstimate(estimate: Estimate): List<Estimate.Calc>
}
