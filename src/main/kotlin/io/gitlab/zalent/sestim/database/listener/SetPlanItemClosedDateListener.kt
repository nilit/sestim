package io.gitlab.zalent.sestim.database.listener

import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import java.time.LocalDateTime
import javax.persistence.PreUpdate

class SetPlanItemClosedDateListener {
    @PreUpdate
    fun setDateClosed(planItemEntity: PlanItemEntity) {
        val isClosed = planItemEntity.isNotActive
        if (isClosed) {
            planItemEntity.dateClosed = LocalDateTime.now()
        }
    }
}
