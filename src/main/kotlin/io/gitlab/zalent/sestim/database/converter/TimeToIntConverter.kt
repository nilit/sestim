package io.gitlab.zalent.sestim.database.converter

import io.gitlab.zalent.sestim.database.Time
import javax.persistence.AttributeConverter
import javax.persistence.Convert

@Convert
class TimeToIntConverter : AttributeConverter<Time, Int> {
    override fun convertToDatabaseColumn(time: Time?) = time?.toMinutesIncludingHours()
    
    override fun convertToEntityAttribute(minutesFromDb: Int?): Time? {
        return if (minutesFromDb != null) {
            Time(minutes = minutesFromDb)
        } else {
            null
        }
    }
}
