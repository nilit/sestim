package io.gitlab.zalent.sestim.database.service

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.repository.EstimateCalcRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class EstimateCalcService {
    @Autowired private lateinit var calcRepo: EstimateCalcRepository
    
    fun getByEstimate(parentEstimate: Estimate): List<Estimate.Calc> {
        val childrenCalcList = calcRepo.getByEstimate(parentEstimate)
        return childrenCalcList.sortedBy { it.dateCreated }
    }

    fun create(parentEstimate: Estimate): Estimate.Calc {
        val childrenCalc = Estimate.Calc(estimate = parentEstimate)
        calcRepo.save(childrenCalc)
        return childrenCalc
    }
    
    fun save(calc: Estimate.Calc): Estimate.Calc = calcRepo.save(calc)
    
    fun delete(calc: Estimate.Calc) = calcRepo.delete(calc.id)
}
