package io.gitlab.zalent.sestim.database.repository

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import org.springframework.data.repository.CrudRepository

interface StoryRepository : CrudRepository<Story, ID> {
    fun findByProject(project: Project): List<Story>
}
