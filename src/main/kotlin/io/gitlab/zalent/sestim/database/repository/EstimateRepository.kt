package io.gitlab.zalent.sestim.database.repository

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.entity.Task
import org.springframework.data.jpa.repository.JpaRepository

interface EstimateRepository : JpaRepository<Estimate, ID> {
    fun findByProject(project: Project): List<Estimate>
    fun findByStory(story: Story): List<Estimate>
    fun findByTask(task: Task): List<Estimate>
}
