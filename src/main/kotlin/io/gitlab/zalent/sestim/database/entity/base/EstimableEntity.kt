package io.gitlab.zalent.sestim.database.entity.base

import io.gitlab.zalent.sestim.database.Time

interface EstimableEntity {
    var best: Time
    var worst: Time
    var expected: Time
    var actual: Time
}
