package io.gitlab.zalent.sestim.database.listener

import io.gitlab.zalent.sestim.database.entity.base.DatedEntity
import java.time.LocalDateTime
import javax.persistence.PreUpdate

class UpdateDateModifiedListener {
    @PreUpdate
    fun updateDateModified(entity: DatedEntity) {
        entity.dateModified = LocalDateTime.now()
    }
}
