package io.gitlab.zalent.sestim.database.listener

import io.gitlab.zalent.sestim.database.entity.Project
import java.time.LocalDateTime
import javax.persistence.PreUpdate

class SetProjectClosedDateListener {
    @PreUpdate
    fun setDateClosed(project: Project) {
        val isClosed = project.state == Project.State.Closed
        val isArchived = project.state == Project.State.Archived
        if (isClosed or isArchived) {
            project.dateClosed = LocalDateTime.now()
        }
    }
}
