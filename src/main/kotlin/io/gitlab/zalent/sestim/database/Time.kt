package io.gitlab.zalent.sestim.database

import io.gitlab.zalent.sestim.database.entity.Estimate
import java.time.Duration
/**
 * Wraps [Duration] providing a more convenient interface for
 * the [Estimate]'s time format, which is: `HHH:mm`.
 */
class Time {
    var hours: Int
        get() = duration.toHours().toInt()
        set(value) {
            val minutesCurrent = minutes.toLong()
            val durationNewWithoutMinutes = Duration.ofHours(value.toLong())
            duration = durationNewWithoutMinutes
            duration = duration.plusMinutes(minutesCurrent)
        }

    var minutes: Int
        get() {
            val minutesIncludingHours = duration.toMinutes().toInt()
            val minutesExcludingHours = minutesIncludingHours % 60
            return minutesExcludingHours
        }
        set(value) {
            val hoursCurrent = hours.toLong()
            val durationNewWithoutHours = Duration.ofMinutes(value.toLong())
            duration = durationNewWithoutHours
            duration = duration.plusHours(hoursCurrent)
        }

    private var duration: Duration

    constructor(hours: Int = 0, minutes: Int = 0) {
        duration = Duration.ofHours(hours.toLong())
        duration = duration.plusMinutes(minutes.toLong())
    }

    fun toMinutesIncludingHours() = duration.toMinutes().toInt()

    operator fun plusAssign(timeToAdd: Time) {
        hours += timeToAdd.hours
        minutes += timeToAdd.minutes
    }
    
    override fun toString(): String {
        val hoursStrFormatted = leftPad(hours, 3)
        val minutesStrFormatted = leftPad(minutes, 2)
        return "$hoursStrFormatted:$minutesStrFormatted"
    }

    override fun equals(other: Any?): Boolean = when (other) {
        is Time -> other.toString() == this.toString()
        else -> super.equals(other)
    }

    override fun hashCode(): Int = duration.hashCode()
    
    private fun leftPad(num: Int, padding: Int) = String.format("%0${padding}d", num)
}
