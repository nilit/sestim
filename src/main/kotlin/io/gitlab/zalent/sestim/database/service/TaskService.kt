package io.gitlab.zalent.sestim.database.service

import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.entity.Task
import io.gitlab.zalent.sestim.database.repository.TaskRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class TaskService {
    @Autowired private lateinit var taskRepo: TaskRepository

    fun getByStory(story: Story): List<Task> = taskRepo.findByStory(story)

    fun save(task: Task): Task = taskRepo.save(task)

    fun delete(task: Task) = taskRepo.delete(task.id)
}
