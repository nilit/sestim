package io.gitlab.zalent.sestim.database.service

import io.gitlab.zalent.sestim.database.entity.Estimate
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.entity.Task
import io.gitlab.zalent.sestim.database.repository.EstimateRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class EstimateService {
    @Autowired private lateinit var estimateRepo: EstimateRepository
    
    fun getByEstimateOwner(ownerEntity: EstimateOwnerEntity): List<Estimate> {
        val estimates = when(ownerEntity) {
            is Project -> estimateRepo.findByProject(ownerEntity)
            is Story -> estimateRepo.findByStory(ownerEntity)
            is Task -> estimateRepo.findByTask(ownerEntity)
            else -> throw IllegalArgumentException()
        }
        return estimates.sortedBy { it.dateCreated }
    }
    
    fun create(ownerEntity: EstimateOwnerEntity): Estimate {
        val estimateNew = when (ownerEntity) {
            is Project -> Estimate(project = ownerEntity)
            is Story -> Estimate(story = ownerEntity)
            is Task -> Estimate(task = ownerEntity)
            else -> throw IllegalArgumentException()
        }
        estimateRepo.save(estimateNew)
        return estimateNew
    }

    fun save(estimate: Estimate): Estimate = estimateRepo.save(estimate)
    
    fun delete(estimate: Estimate) {
        estimateRepo.delete(estimate.id)
    }
}
