package io.gitlab.zalent.sestim.database.service

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.repository.ProjectRepository
import io.gitlab.zalent.sestim.database.repository.StoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class ProjectService {
    @Autowired private lateinit var projectRepo: ProjectRepository
    @Autowired private lateinit var storyRepo: StoryRepository
    
    fun getAll(): List<Project> = projectRepo.findAll()

    fun save(project: Project): Project = projectRepo.save(project)

    fun delete(project: Project) {
        deleteProjectStories(project)
        projectRepo.delete(project.id)
    }

    /**
     * It is possible to do that from hibernate, but I believe that would
     * be even more ugly.
     */
    private fun deleteProjectStories(project: Project) {
        val projectStories = storyRepo.findByProject(project)
        projectStories.forEach { storyRepo.delete(it) }
    }
}
