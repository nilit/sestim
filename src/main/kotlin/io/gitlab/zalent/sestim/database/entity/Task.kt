package io.gitlab.zalent.sestim.database.entity

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.base.DatedEntity
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.database.listener.SetPlanItemClosedDateListener
import io.gitlab.zalent.sestim.database.listener.UpdateDateModifiedListener
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["story_id", "name"])])
@Entity(name = "tasks")
@EntityListeners(UpdateDateModifiedListener::class, SetPlanItemClosedDateListener::class)
class Task(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: ID? = null,

    @Column(nullable = false)
    var name: String = "",

    var description: String = "",

    var pubId: ID = 0,

    var LOC: Long = 0,

    @Enumerated(EnumType.ORDINAL)
    var type: PlanItemEntity.Type = PlanItemEntity.Type.NotSet,

    @Enumerated(EnumType.ORDINAL)
    var size: PlanItemEntity.Size = PlanItemEntity.Size.NotSet,

    @Enumerated(EnumType.ORDINAL)
    override var state: PlanItemEntity.State = PlanItemEntity.State.ReadyForDev,

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    var story: Story = Story(),

    override val dateCreated: LocalDateTime = LocalDateTime.now(),
    override var dateModified: LocalDateTime = LocalDateTime.now(),
    override var dateClosed: LocalDateTime? = null
) : PlanItemEntity(), EstimateOwnerEntity, DatedEntity
