package io.gitlab.zalent.sestim.database.service

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.repository.ProjectTypeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class ProjectTypeService {
    @Autowired private lateinit var repo: ProjectTypeRepository
    
    fun save(projectType: Project.Type): Project.Type = repo.save(projectType)

    fun delete(projectType: Project.Type) = repo.delete(projectType.id)
}
