package io.gitlab.zalent.sestim.database.entity

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.base.DatedEntity
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.entity.base.PlanItemEntity
import io.gitlab.zalent.sestim.database.listener.SetPlanItemClosedDateListener
import io.gitlab.zalent.sestim.database.listener.UpdateDateModifiedListener
import io.gitlab.zalent.sestim.database.repository.StoryRepository
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import java.sql.Date as SqlDate

@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["project_id", "name"])])
@Entity(name = "stories")
@EntityListeners(UpdateDateModifiedListener::class, SetPlanItemClosedDateListener::class)
class Story(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: ID? = null,

    /**
     * Don't try set it to null, or it will break [StoryRepository.findByProject].
     */
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    var project: Project = Project(),

    @Column(nullable = false)
    var name: String = "",

    var description: String = "",

    var pubId: ID = 0,

    var LOC: Long = 0,

    @Enumerated(EnumType.ORDINAL)
    var type: PlanItemEntity.Type = PlanItemEntity.Type.Feature,

    @Enumerated(EnumType.ORDINAL)
    var size: PlanItemEntity.Size = PlanItemEntity.Size.NotSet,

    @Enumerated(EnumType.ORDINAL)
    override var state: PlanItemEntity.State = PlanItemEntity.State.ReadyForDev,

    override val dateCreated: LocalDateTime = LocalDateTime.now(),
    override var dateModified: LocalDateTime = LocalDateTime.now(),
    override var dateClosed: LocalDateTime? = null
) : PlanItemEntity(), EstimateOwnerEntity, DatedEntity {
    
    override fun toString(): String = name

    override fun equals(other: Any?): Boolean = when (other) {
        is Story -> { other.id == this.id }
        else -> super.equals(other)
    }
}
