package io.gitlab.zalent.sestim.database.service

import io.gitlab.zalent.sestim.database.entity.Project
import io.gitlab.zalent.sestim.database.entity.Story
import io.gitlab.zalent.sestim.database.repository.StoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.transaction.Transactional

@Service
@Transactional
class StoryService {
    @Autowired private lateinit var storyRepo: StoryRepository
    @Autowired private lateinit var entityManager: EntityManager

    fun getByProject(project: Project): List<Story> = storyRepo.findByProject(project)

    fun save(story: Story) {
        storyRepo.save(story)
    }

    fun delete(story: Story) {
        entityManager.merge(story)
        storyRepo.delete(story.id)
    }
}
