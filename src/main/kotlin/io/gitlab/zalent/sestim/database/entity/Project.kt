package io.gitlab.zalent.sestim.database.entity

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.entity.base.ClosableEntity
import io.gitlab.zalent.sestim.database.entity.base.DatedEntity
import io.gitlab.zalent.sestim.database.entity.base.EstimateOwnerEntity
import io.gitlab.zalent.sestim.database.listener.SetProjectClosedDateListener
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

// TODO allow to ignore dateClosed
@Suppress("EqualsOrHashCode")
@Entity(name = "projects")
@EntityListeners(SetProjectClosedDateListener::class)
class Project(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: ID? = null,

    // TODO use null in place of 
    @Column(nullable = false, unique = true)
    var name: String = "",

    @ManyToOne
    var type: Project.Type? = null,

    // TODO rename to LOC
    var KLOC: Long? = null,

    var pubId: ID? = null,

    var color: Int? = null,

    @Enumerated(EnumType.ORDINAL)
    var state: State = State.Open,

    override val dateCreated: LocalDateTime = LocalDateTime.now(),
    override var dateModified: LocalDateTime = LocalDateTime.now(),
    override var dateClosed: LocalDateTime? = null
) : EstimateOwnerEntity, DatedEntity, ClosableEntity {
    
    @Entity(name = "project_types")
    class Type(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: ID? = null,
    
        @Column(nullable = false, unique = true)
        var name: String? = null
    ) {
        override fun toString(): String = name.toString()
        
        override fun equals(other: Any?): Boolean = when (other) {
            is Type -> { other.id == this.id }
            else -> super.equals(other)
        }
    }
    
    enum class State {
        Open,
        Closed,
        Stalled,
        Archived,
    }

    override fun toString(): String = name

    /**
     * Otherwise there will be problems with comparision of two instances of the same
     * entity.
     */
    override fun equals(other: Any?): Boolean = when (other) {
        is Project -> { other.id == this.id }
        else -> super.equals(other)
    }
}
