package io.gitlab.zalent.sestim.database.entity

import io.gitlab.zalent.sestim.database.ID
import io.gitlab.zalent.sestim.database.Time
import io.gitlab.zalent.sestim.database.converter.TimeToIntConverter
import io.gitlab.zalent.sestim.database.entity.base.DatedEntity
import io.gitlab.zalent.sestim.database.entity.base.EstimableEntity
import io.gitlab.zalent.sestim.database.listener.UpdateDateModifiedListener
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDateTime
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity(name = "estimates")
@EntityListeners(UpdateDateModifiedListener::class)
class Estimate(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: ID? = null,

    @ManyToOne(optional = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    var project: Project? = null,

    @ManyToOne(optional = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    var story: Story? = null,

    @ManyToOne(optional = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    var task: Task? = null,

    @Convert(converter = TimeToIntConverter::class)
    override var best: Time = Time(),

    @Convert(converter = TimeToIntConverter::class)
    override var worst: Time = Time(),

    @Convert(converter = TimeToIntConverter::class)
    override var expected: Time = Time(),

    @Convert(converter = TimeToIntConverter::class)
    override var actual: Time = Time(),

    var isDateValid: Boolean = true,

    var isAdjusted: Boolean = true,

    override val dateCreated: LocalDateTime = LocalDateTime.now(),
    override var dateModified: LocalDateTime = LocalDateTime.now()
) : DatedEntity, EstimableEntity {
    
    val isSufficientlyFilled: Boolean
        get() {
            val timeZero = Time()
            return (this.best != timeZero) and
                (this.worst != timeZero) and
                (this.expected != timeZero)
        }
    
    @Entity(name = "estimate_calculations")
    @EntityListeners(UpdateDateModifiedListener::class)
    class Calc(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: ID? = null,
        
        @ManyToOne
        @OnDelete(action = OnDeleteAction.CASCADE)
        var estimate: Estimate? = null,
        
        @Convert(converter = TimeToIntConverter::class)
        override var best: Time = Time(),
        
        @Convert(converter = TimeToIntConverter::class)
        override var worst: Time = Time(),
        
        @Convert(converter = TimeToIntConverter::class)
        override var expected: Time = Time(),
        
        @Convert(converter = TimeToIntConverter::class)
        override var actual: Time = Time(),
    
        val dateCreated: LocalDateTime = LocalDateTime.now()
    ) : EstimableEntity {
        
        operator fun plusAssign(estimate: EstimableEntity?) {
            if (estimate == null) return
            
            this.best += estimate.best
            this.worst += estimate.worst
            this.expected += estimate.expected
            this.actual += estimate.actual
        }
    }
    
    constructor(
        bestHours: Int, worstHours: Int = 0, expectedHours: Int = 0, actualHours: Int = 0
    ) : this() {
        this.best = Time(hours = bestHours)
        this.worst = Time(hours = worstHours)
        this.expected = Time(hours = expectedHours)
        this.actual = Time(hours = actualHours)
    }
}
