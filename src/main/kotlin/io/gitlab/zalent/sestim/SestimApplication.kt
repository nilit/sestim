package io.gitlab.zalent.sestim

import io.gitlab.zalent.sestim.config.configureHeadlessMode
import io.gitlab.zalent.sestim.gui.view.ViewDocker
import javafx.scene.image.Image
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.ApplicationContext
import org.springframework.context.ConfigurableApplicationContext
import tornadofx.*
import kotlin.reflect.KClass

@SpringBootApplication
class SestimApplication : App(ViewDocker::class) {
    lateinit var context: ConfigurableApplicationContext

    override fun init() {
        configureHeadlessMode()
        
        setStageIcon(Image("/img/icon.png"))
        
        context = buildContext()
        `init JavaFX dependency injector`(context)
    }
    
    fun buildContext() = SpringApplicationBuilder(this::class.java).web(false).run()

    fun `init JavaFX dependency injector`(context: ApplicationContext) {
        FX.dicontainer = object : DIContainer {
            override fun <T : Any> getInstance(type: KClass<T>): T {
                return context.getBean(type.java)
            }
            override fun <T : Any> getInstance(type: KClass<T>, name: String): T {
                return context.getBean(type.java, name)
            }
        }
    }
}
